<?php
namespace app\shop\validate;
use think\Validate;

class ShopRecharge extends Validate
{
    protected $rule = [
        'pic_id' => 'require',
        'remarks'=>'require',
        'price'=>['require','regex'=>'/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/','gt'=>0],
    ];

    protected $message = [
        'pic_id.require' => '支付截图不能为空',
        'remarks.require' => '支付类型不能为空',
        'price.require' => '充值金额不能为空',
        'price.regex' => '充值金额格式错误',
        'price.gt' => '充值金额需大于0'
    ];

}