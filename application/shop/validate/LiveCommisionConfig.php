<?php
namespace app\shop\validate;
use think\Validate;

class LiveCommisionConfig extends Validate
{
    protected $rule = [
        'gift_commision_rate' => 'require',
        'goods_commision_rate' => 'require',
        'shops_id' => 'require|number|unique:LiveCommisionConfig',
    ];

    protected $message = [
        'gift_commision_rate.require' => '礼物分成比例不能为空',
        'goods_commision_rate.require' => '商品销售分成比例不能为空',
        'shop_id.require' => '店铺id不能为空',
        'shops_id.unique' => '该店铺已设置，请勿重复设置',
        'shops_id.number' => '店铺id一定要为数字！',
    ];

}