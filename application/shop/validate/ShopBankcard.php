<?php
namespace app\shop\validate;
use think\Validate;

class ShopBankcard extends Validate

{
    protected $rule = [
        'name' => 'require',
        'telephone' => 'require',
        'card_number' => 'require',
        'bank_name' => 'require',
        'shop_id' => ['require','regex'=>'/^\+?[1-9][0-9]*$/'],
    ];

    protected $message = [
        'name.require' => '真实姓名不能为空',
        'telephone.require' => '手机号不能为空',
        'card_number.require' => '请填写银行卡号',
        'bank_name.require' => '请填写所属银行名称',
        'shop_id.require' => '缺少商家参数',
        'shop_id.regex' => '缺少商家参数',
    ];



}
