<?php
namespace app\shop\validate;
use think\Validate;

class ShopShdz extends Validate

{
    protected $rule = [
        'name' => 'require|max:10',
        'telephone'=>'require',
        'address'=>'require|max:50',
        'shop_id' => ['require','regex'=>'/^\+?[1-9][0-9]*$/'],
    ];

    protected $message = [
        'name.require' => '请填写收货人姓名',
        'name.max' => '收货人姓名最多10个字符',
        'telephone.require' => '请填写联系手机号',
        'address.require' => '详细地址不能为空',
        'address.max' => '详细地址最多50个字符',
        'shop_id.require' => '缺少商家参数',
        'shop_id.regex' => '缺少商家参数',
    ];
    

}