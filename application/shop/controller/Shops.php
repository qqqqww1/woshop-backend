<?php
namespace app\shop\controller;
use app\shop\controller\Common;
use think\Db;

class Shops extends Common
{
    public function info(){
        if(request()->isPost()){
            $admin_id = session('shopadmin_id');
            $shop_id = session('shopsh_id');
            $data = input('post.');
            $data['id'] = $shop_id;
            $result = $this->validate($data,'Shops');
            if(true !== $result){
                $value = array('status'=>0,'mess'=>$result);
            }else{
                $shops = Db::name('shops')->where('id',$shop_id)->field('id,logo')->find();
                if($shops){
                $data['latlon'] = str_replace('，', ',', $data['latlon']);
                if(strpos($data['latlon'],',') !== false){

                    $latlon = explode(',', $data['latlon']);

                    if(!empty($data['pic_id'])){
                        $data['logo'] = $data['pic_id'];
                    }else{
                        if(!empty($shops['logo'])){
                            $data['logo'] = $shops['logo'];
                        }else{
                            $data['logo'] = '';
                        }
                    }

                    if(empty($data['wxnum'])){
                        $data['wxnum'] = '';
                    }

                    if(empty($data['qqnum'])){
                        $data['qqnum'] = '';
                    }

                    if(empty($data['sertime'])){
                        $data['sertime'] = '';
                    }

                    $data['search_keywords'] = str_replace('，', ',', $data['search_keywords']);

                    $count = Db::name('shops')->update(array(
                        'shop_name'=>$data['shop_name'],
                        'shop_desc'=>$data['shop_desc'],
                        'search_keywords'=>$data['search_keywords'],
                        'contacts'=>$data['contacts'],
                        'telephone'=>$data['telephone'],
                        'wxnum'=>$data['wxnum'],
                        'qqnum'=>$data['qqnum'],
                        'logo'=>$data['logo'],
                        'sertime'=>$data['sertime'],
                        'pro_id'=>$data['pro_id'],
                        'city_id'=>$data['city_id'],
                        'area_id'=>$data['area_id'],
                        'address'=>$data['address'],
                        'lng'=>$latlon[0],
                        'lat'=>$latlon[1],
                        'fenxiao'=>$data['fenxiao'],
                        'id'=>$data['id']
                    ));
                    if($count !== false){
                        $value = array('status'=>1,'mess'=>'保存信息成功');
                    }else{
                        $value = array('status'=>0,'mess'=>'保存信息失败');
                    }
                }else{
                    $value = array('status'=>0,'mess'=>'地址坐标参数错误');
                }

                }else{
                    $value = array('status'=>0,'mess'=>'找不到相关商店信息');
                }
            }
            return json($value);
        }else{
            $admin_id = session('shopadmin_id');
            $shop_id = session('shopsh_id');
            $shops = Db::name('shops')->alias('a')->field('a.*,b.industry_name,b.remind,c.pro_name,d.city_name,u.area_name')->join('sp_industry b','a.indus_id = b.id','LEFT')->join('sp_province c','a.pro_id = c.id','LEFT')->join('sp_city d','a.city_id = d.id','LEFT')->join('sp_area u','a.area_id = u.id','LEFT')->where('a.id',$shop_id)->where('a.open_status',1)->find();
            if($shops){
                
                $prores = Db::name('province')->where('checked',1)->where('pro_zs',1)->field('id,pro_name,zm')->order('sort asc')->select();
                $cityres = Db::name('city')->where('pro_id',$shops['pro_id'])->field('id,city_name,zm')->order('sort asc')->select();
                $areares = Db::name('area')->where('city_id',$shops['city_id'])->field('id,area_name,zm')->select();
                $this->assign('shops',$shops);
                $this->assign('prores',$prores);
                $this->assign('cityres',$cityres);
                $this->assign('areares',$areares);
                return $this->fetch();
            }else{
                $this->error('找不到相关信息','index/index');
            }
        }
    }
    
    public function getcitylist(){
        if(request()->isPost()){
            $pro_id = input('post.pro_id');
            if($pro_id){
                $cityres = Db::name('city')->where('pro_id',$pro_id)->where('checked',1)->where('city_zs',1)->field('id,city_name,zm')->order('sort asc')->select();
                if(empty($cityres)){
                    $cityres = 0;
                }
                return json($cityres);
            }
        }
    }
    
    public function getarealist(){
        if(request()->isPost()){
            $city_id = input('post.city_id');
            if($city_id){
                $areares = Db::name('area')->where('city_id',$city_id)->where('checked',1)->field('id,area_name,zm')->order('sort asc')->select();
                if(empty($areares)){
                    $areares = 0;
                }
                return json($areares);
            }
        }
    }


}