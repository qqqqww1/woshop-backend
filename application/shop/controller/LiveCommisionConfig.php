<?php
namespace app\shop\controller;
use app\shop\controller\Common;
use think\Db;

class LiveCommisionConfig extends Common{
    
    public function lst(){
        $shop_id = session('shopsh_id');

        $list = Db::name('live_commision_config a')->join('shops b','a.shop_id = b.id')->where('shop_id',$shop_id)->field('a.*,b.shop_name')->select();
        $this->assign('list',$list);// 赋值数据集
        return $this->fetch();
    }

    public function edit(){
        if(request()->isPost()){
            if(input('post.id')){
                $data = input('post.');
                $result = $this->validate($data,'LiveCommisionConfig');
                if(true !== $result){
                    $value = array('status'=>0,'mess'=>$result);
                }else{
                    $liveCommisionConfigId = Db::name('live_commision_config')->where('id',$data['id'])->field('id')->find();
                    if($liveCommisionConfigId){
                        $count = db('live_commision_config')->update($data);
                        if($count !== false){
                            ys_admin_logs('编辑直播分成配置','LiveCommisionConfig',$data['id']);
                            $value = array('status'=>1,'mess'=>'编辑成功');
                        }else{
                            $value = array('status'=>0,'mess'=>'编辑失败');
                        }
                    }else{
                        $value = array('status'=>0,'mess'=>'找不到相关信息，编辑失败');
                    }
                }
            }else{
                $value = array('status'=>0,'mess'=>'缺少参数，编辑失败');
            }
            return json($value);
        }else{
            if(input('id')){
                $id = input('id');
                $liveCommisionConfigs = Db::name('LiveCommisionConfig')->find($id);
                $shopsNameRes = db('shops')->where(['normal'=>1])->whereNotIn('id','1')->field('id,shop_name')->select();
                $this->assign('shopsNameRes',$shopsNameRes);// 赋值数据集
                if($liveCommisionConfigs){
                    $this->assign('liveCommisionConfig', $liveCommisionConfigs);
                    return $this->fetch();
                }else{
                    $this->error('找不到相关信息1');
                }
            }else{
                $this->error('缺少参数');
            }
        }
    }

    //礼物打赏记录
    public function liveGiftGive(){
        $shop_id = session('shopsh_id');
        $limit = input('param.limit/d', 15);
        $keyword = input('param.keyword');
        $where=[];
        if ($keyword) {
            $where['b.shop_name|d.user_name|e.user_name'] = ['like', "%{$keyword}%"];
        }
        $where['a.shop_id'] = $shop_id;
        $list = db('live_gift_give a')
            ->join('shops b','a.shop_id = b.id')
            ->join('live_gift c','a.gid = c.id')
            ->join('member d','a.uid = d.id')
            ->join('member e','a.anchor_user_id = e.id')
            ->field('a.*,b.shop_name,c.name,c.pic,d.user_name,d.headimgurl,d.phone,e.user_name anchor_user_name')
            ->where($where)
            ->order('a.createtime desc')
            ->paginate($limit);
        $page = $list->render();
        $this->assign([
            'list'=>$list,
            'page'=>$page
        ]);
        return $this->fetch();
    }
}