<?php
namespace app\shop\controller;
use app\shop\controller\Common;
use think\Db;

class Member extends Common{

    public function blacklst(){
        $shop_id = session('shopsh_id');
        $list = Db::name('live_room_block')->alias('a')
        ->field('a.*,b.user_name,b.phone')
        ->join('sp_member b','a.user_id = b.id','LEFT')
        ->where('a.shop_id',$shop_id)->order('a.add_time desc')->select();
        //$page = $list->render();
        if(input('page')){
            $pnum = input('page');
        }else{
            $pnum = 1;
        }
        $this->assign('pnum',$pnum);// 赋值分页输出
        $this->assign('list',$list);// 赋值分页输出
        if(request()->isAjax()){
            return $this->fetch('ajaxpage');
        }else{
            return $this->fetch('blacklst');
        }
    }

    public function delblack(){
        if(input('id') && !is_array(input('id'))){
            $shop_id = session('shopsh_id');
            $id = input('id');
            $block = Db::name('live_room_block')->where('id',$id)->where('shop_id',$shop_id)->field('id')->find();
            if($block){
                // 启动事务
                Db::startTrans();
                try{
                    Db::name('live_room_block')->where('id',$id)->delete();
                    // 提交事务
                    Db::commit();
                    $value = array('status'=>1,'mess'=>'删除成功');
                } catch (\Exception $e) {
                    // 回滚事务
                    Db::rollback();
                    $value = array('status'=>0,'mess'=>'删除失败');
                }
            }else{
                $value = array('status'=>0,'mess'=>'操作失败');
            }
        }else{
            $value = array('status'=>0,'mess'=>'删除失败');
        }
        return json($value);
    }

    public function service(){
        if(request()->isPost()) {
            $shopId = session('shopsh_id');
            if(!input('post.new_phone')){
                $value = array('status' => 0, 'mess' => '请填写新客服手机号码');
                return json($value);
            }
            $newPhone = input('post.new_phone');
            if (!input('post.phonecode')) {
                $value = array('status' => 0, 'mess' => '请填写手机验证码');
                return json($value);
            }
            $code = input('post.phonecode');
            $smsCode = new SmsCode();
            $checkSmsCode = $smsCode->checkSmsCode($code, $newPhone, 5);
            if ($checkSmsCode['status'] == 1) {
                // 启动事务
                Db::startTrans();
                try {
                    $memberDb = Db::name('member');
                    $memberId = $memberDb->where('shop_id',$shopId)->value('id');
                    $memberDb->update(['id'=>$memberId,'shop_id'=>0]);
                    $memberDb->where('phone',$newPhone)->update(['shop_id'=>$shopId]);
                    // 提交事务
                    Db::commit();
                    $value = array('status' => 1, 'mess' => '修改客服账号成功');
                } catch (\Exception $e) {
                    // 回滚事务
                    Db::rollback();
                    $value = array('status' => 0, 'mess' => '修改客服账号失败');
                }
            } else {
                $value = array('status' => 0, 'mess' => $checkSmsCode['mess']);
            }
            return json($value);
        }
        $shopId = session('shopsh_id');
        $memberInfo = Db::name('member')->where('shop_id', $shopId)->field('id,user_name,phone,qrcodeurl')->find();
        $peizhi = Db::name('config')->where('ename', 'messtime')->field('value')->find();
        $this->assign('messtime', $peizhi['value']);
        $this->assign('memberInfo', $memberInfo);
        return $this->fetch();
    }
}
