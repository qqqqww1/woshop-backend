<?php
namespace app\shop\controller;
use think\Controller;
use think\Db;

class Common extends Controller{
    public $webconfig;
    
    Public function _initialize(){
        if(!session('shopadmin_id') || !session('shopsh_id')){
            $token = input('get.token');
            if($token){
                $shopadminId = db('member_token')->where('token',input('get.token'))->value('shop_admin_id');
                $shopshId  = db('shop_admin')->where('id',$shopadminId)->value('shop_id');
                if($shopadminId && $shopshId){
                    session('shopadmin_id',$shopadminId);
                    session('shopsh_id',$shopshId);
                    session('client','mobile');
                }else{
//                    $this->redirect('login/tips');
                    echo "<script>alert('token错误');</script>";
                    die;
                }
            }else{
                $this->redirect('login/index');
            }

        }
        $this->_getconfig();
    }
    
    public function _getconfig(){
        $_configres = Db::name('config')->field('ename,value')->select();
        $configres = array();
        foreach ($_configres as $v){
            $configres[$v['ename']] = $v['value'];
        }
        $this->webconfig=$configres;
        $this->assign('configres',$configres);
    }
    
}