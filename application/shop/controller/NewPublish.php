<?php

namespace app\shop\controller;
use app\shop\controller\Common;
use app\common\Lookup;
use app\shop\model\NewPublish as NewPublishModel;
use app\shop\model\Goods;
use app\shop\model\Video;
use think\Db;

class NewPublish extends Common{
    
    public $newModel;
    
    public function _initialize() {
        parent::_initialize();
        $this->newModel = new NewPublishModel();
    }
    
    public function lst() {
        $keyword = input('keyword');
        $list = $this->newModel->getNewPublishList($keyword, Lookup::pageSize);
        $goodsModel = new Goods();
        $videoModel = new Video();
        $cos_domain = $this->webconfig['cos_domain'];
        foreach ($list as $key => $item) {
            $goods_list = $goodsModel->getGoodsInfo($item['goods_id']);
            $video_list = $videoModel->getVideoListByIds($item['video_id']);
            $goods_name_str = '';
            $vide_path_str = '';
            foreach ($goods_list as $v) {
                 $goods_name_str .= '【' . $v['goods_name'] . '】<br>';
            }
            foreach ($video_list as $v) {
                 $vide_path_str .= "<a href='{$v['video_path']}' target='_bank'>..." . $v['video_path'] . '</a><br>';
            }
            $list[$key]['goods_name'] = $goods_name_str;
            $list[$key]['video_path'] = $vide_path_str;
        }
        $page = $list->render();
        $data = array(
            'list' => $list,
            'page' => $page,
            'keyword' => $keyword,
        );
        $this->assign($data);
        return request()->isAjax() ? $this->fetch('ajaxpage') : $this->fetch();
    }
    
    public function add() {
        if (request()->isPost()) {
            $data = input('post.');
            $data['shop_id'] = session('shopsh_id');

            $lang = db('lang')->select();
            foreach ($lang as $k => $v){
                if(!empty($data['title_'.$v['lang_code']])){
                    $data['title'] = $data['title_'.$v['lang_code']];
                    break;
                }

            }
            foreach ($lang as $k => $v){
                if(!empty($data['content_'.$v['lang_code']])){
                    $data['content'] = $data['content_'.$v['lang_code']];
                    break;
                }
            }
            $result = $this->validate($data, 'NewPublish');
            if(true !== $result){
                return json(array('status' => Lookup::isClose, 'mess' => $result));
            }
            $count = count($data['goods_id']) + count($data['video_id']);
            if ($count > 9) {
                return json(array('status' => Lookup::isClose, 'mess' => '商品和视频共计不超过9个'));
            }
            $data['goods_id'] = implode(',', $data['goods_id']);
            $data['video_id'] = implode(',', $data['video_id']);
            $newData['title'] = $data['title'];
            $newData['content'] = $data['content'];
            $newData['shop_id'] = $data['shop_id'];
            $newData['goods_id'] = $data['goods_id'];
            $newData['video_id'] = $data['video_id'];
            $newData['praise_num'] = $data['praise_num'];
            $newData['read_num'] = $data['read_num'];
            $newData['status'] = $data['status'];
            $newData['create_time'] = date('Y-m-d H:i:s',time());
            $newPublishId = db('new_publish')->insertGetId($newData);

            $newPublishLangDb = db('new_publish_lang');
            $langs = db('lang')->select();
            foreach ($langs as $k => $v) {
                $newPublishLangData = [];
                $newPublishLangData['new_publish_id'] = $newPublishId;
                $newPublishLangData['lang_id']  = $v['id'];
                $newPublishLangData['title'] = $data['title_'. $v['lang_code']];
                $newPublishLangData['content'] = $data['content_'. $v['lang_code']];
                // 判断商品多语言设置是否存在，没有就新增，存在就更新
                $newPublishLangDb->insertGetId($newPublishLangData);
            }

            return json(array('status' => Lookup::isOpen,'mess' => '添加成功'));
        }
        $langs = Db::name('lang')->order('id asc')->select();
        $this->assign('langs', $langs);
        return $this->fetch();
    }
    
    public function edit() {
        if (request()->isPost()) {
            $data = input('post.');
            $data['shop_id'] = session('shopsh_id');

            $lang = db('lang')->select();
            foreach ($lang as $k => $v){
                if(!empty($data['title_'.$v['lang_code']])){
                    $data['title'] = $data['title_'.$v['lang_code']];
                    break;
                }

            }
            foreach ($lang as $k => $v){
                if(!empty($data['content_'.$v['lang_code']])){
                    $data['content'] = $data['content_'.$v['lang_code']];
                    break;
                }
            }

            $result = $this->validate($data, 'NewPublish');
            if(true !== $result){
                return json(array('status' => Lookup::isClose, 'mess' => $result));
            }
            $count = count($data['goods_id']) + count($data['video_id']);
            if ($count > 9) {
                return json(array('status' => Lookup::isClose, 'mess' => '商品和视频共计不超过9个'));
            }
            $data['goods_id'] = implode(',', $data['goods_id']);
            $data['video_id'] = implode(',', $data['video_id']);

            $newData['title'] = $data['title'];
            $newData['content'] = $data['content'];
            $newData['shop_id'] = $data['shop_id'];
            $newData['goods_id'] = $data['goods_id'];
            $newData['video_id'] = $data['video_id'];
            $newData['praise_num'] = $data['praise_num'];
            $newData['read_num'] = $data['read_num'];
            $newData['status'] = $data['status'];
            $newData['id'] = $data['id'];

            db('new_publish')->update($newData);

            $newPublishLangDb = db('new_publish_lang');
            $langs = db('lang')->select();
            foreach ($langs as $k => $v) {
                $newPublishLangData = [];
                $newPublishLangData['new_publish_id'] = $data['id'];
                $newPublishLangData['lang_id']  = $v['id'];
                $newPublishLangData['title'] = $data['title_'. $v['lang_code']];
                $newPublishLangData['content'] = $data['content_'. $v['lang_code']];
                $newPublishLangDb->where(['new_publish_id'=>$data['id'],'lang_id'=>$v['id']])->update($newPublishLangData);
            }
            return json(array('status' => Lookup::isOpen,'mess' => '编辑成功'));
        }
        $id = input('id');
        $info = $this->newModel->getNewPublishInfo($id);
        $goodsModel = new Goods();
        $goods_list = $goodsModel->getGoodsListByIds($info['goods_id']);

        $videoModel = new Video();
        $video_list = $videoModel->getVideoListByIds($info['video_id']);

        foreach ($video_list as $key => $v) {
            $video_list[$key]['video_url'] = $v['video_path'];
        }
        $newPublishLangs = Db::name('new_publish_lang')->where('new_publish_id',$id)->select();
        $langs = Db::name('lang')->order('id asc')->select();
        $this->assign('langs', $langs);
        $this->assign('newPublishLangs', $newPublishLangs);
        $data = array(
            'info' => $info,
            'goods_list' => $goods_list,
            'video_list' => $video_list
        );
        $this->assign($data);
        return $this->fetch();
    }
    
    public function delete() {
        if (!request()->isAjax()) {
            return json(array('status' => Lookup::isClose, 'mess' => '请求方式错误'));
        }
        $id = input('id');
        if (!$id) {
            return json(array('status' => Lookup::isClose, 'mess' => '参数错误'));
        }
        $delResult = $this->newModel->destroy($id);
        if (!$delResult) {
            return json(array('status' => Lookup::isClose, 'mess' => '删除失败'));
        }
        return json(array('status' => Lookup::isOpen, 'mess' => '删除成功'));
    }
    
    public function changeStatus() {
        if (!request()->isPost()) {
            return json(array('status' => 0, 'mess' => '请求方式错误'));
        }
        $id = input('post.id');
        if (!$id) {
            return json(array('status' => 0, 'mess' => '参数错误'));
        }
        $status = abs(input('post.status') - 1);
        $data = array('status' => $status);
        $updateResult = $this->newModel->update($data, array('id' => $id));
        if (!$updateResult) {
            return json(array('status' => 0, 'mess' => '修改失败'));
        }
        return json(array('status' => 1, 'mess' => '修改成功'));
    }
}
