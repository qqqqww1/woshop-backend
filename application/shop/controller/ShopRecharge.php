<?php
namespace app\shop\controller;
use app\shop\controller\Common;
use think\Db;

class ShopRecharge extends Common{

    public function lst(){
        $shop_id = session('shopsh_id');
        $list = Db::name('shop_recharge')->where('shop_id',$shop_id)->order('create_time desc')->paginate(25);
        $page = $list->render();

        if(input('page')){
            $pnum = input('page');
        }else{
            $pnum = 1;
        }

        $this->assign(array(
            'list'=>$list,
            'page'=>$page,
            'pnum'=>$pnum
        ));
        if(request()->isAjax()){
            return $this->fetch('ajaxpage');
        }else{
            return $this->fetch('lst');
        }
    }


    public function info(){
        if(input('id')){
            $shop_id = session('shopsh_id');
            $id = input('id');
            $shopRecharge = Db::name('shop_recharge')->where('id',$id)->where('shop_id',$shop_id)->find();
            if($shopRecharge){
                $this->assign('shopRecharge',$shopRecharge);
                return $this->fetch();
            }else{
                $this->error('参数错误');
            }
        }else{
            $this->error('缺少参数');
        }
    }

    public function search(){
        $shop_id = session('shopsh_id');

        if(input('post.cz_zt') != ''){
            cookie("shopcz_zt", input('post.cz_zt'), 7200);
        }

        if(input('post.starttime') != ''){
            $shopczstarttime = strtotime(input('post.starttime'));
            cookie('shopczstarttime',$shopczstarttime,3600);
        }

        if(input('post.endtime') != ''){
            $shoptxendtime = strtotime(input('post.endtime'));
            cookie('shopczendtime',$shoptxendtime,3600);
        }

        $where = array();

        $where['shop_id'] = $shop_id;

        if(cookie('shopcz_zt') != ''){
            $shopcz_zt = (int)cookie('shopcz_zt');
            if($shopcz_zt != 0){
                switch($shopcz_zt){
                    //待审核
                    case 1:
                        $where['checked'] = 0;
                        break;
                    //充值完成
                    case 2:
                        $where['checked'] = 1;
                        break;
                }
            }
        }


        if(cookie('shopczendtime') && cookie('shopczstarttime')){
            $where['create_time'] = array(array('egt',cookie('shopczstarttime')), array('lt',cookie('shopczendtime')));
        }

        if(cookie('shopczstarttime') && !cookie('shopczendtime')){
            $where['create_time'] = array('egt',cookie('shopczstarttime'));
        }

        if(cookie('shopczendtime') && !cookie('shopczstarttime')){
            $where['create_time'] = array('lt',cookie('shopczendtime'));
        }

        $list =  Db::name('shop_recharge')->where($where)->order('create_time desc')->paginate(25);
        $page = $list->render();

        if(input('page')){
            $pnum = input('page');
        }else{
            $pnum = 1;
        }
        $search = 1;

        if(cookie('shopczstarttime') != ''){
            $this->assign('starttime',cookie('shopczstarttime'));
        }

        if(cookie('shopczendtime') != ''){
            $this->assign('endtime',cookie('shopczendtime'));
        }

        if(cookie('shopcz_zt') != ''){
            $this->assign('cz_zt',cookie('shopcz_zt'));
        }

        $this->assign('search',$search);
        $this->assign('pnum', $pnum);
        $this->assign('list', $list);// 赋值数据集
        $this->assign('page', $page);// 赋值分页输出
        if(request()->isAjax()){
            return $this->fetch('ajaxpage');
        }else{
            return $this->fetch('lst');
        }
    }





    public function add(){

        if(request()->isPost()){
            $data = input('post.');
            $result = $this->validate($data,'ShopRecharge');
            if(true !== $result){
                $value = array('status'=>0,'mess'=>$result);
                return json($value);
            }
            $shopId           = session('shopsh_id');
            $data['checked'] = 0;
            $data['source_pic'] = $data['pic_id'];
            $data['shop_id'] = $shopId;
            $data['create_time'] = time();
            unset($data['pic_id']);
            // 启动事务
            Db::startTrans();
            try{
                db('shop_recharge')->insertGetId($data);
                $value = array('status'=>1,'mess'=>'完成充值，等待审核');
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                $value = array('status'=>0,'mess'=>'充值失败');
            }
            return json($value);
        }else{
            $info  = db('pt_account')->where('id',1)->find();
            $this->assign('info',$info);
            return $this->fetch();
        }
    }

}
