<?php
namespace app\shop\controller;
use app\shop\controller\Common;
use think\Db;

class ShopAdmin extends Common{
    public function editpwd(){
        $admin_id = session('shopadmin_id');
        if(request()->isPost()){
            if(input('post.old_password')){
                if(input('post.password')){
                    if(input('post.confirm_password')){
                        $old_password = input('post.old_password');
                        $password = input('post.password');
                        $confirm_password = input('post.confirm_password');
                        $userName = input('post.user_name');
                        $shopAdmin = db('shop_admin')->where('user_name',$userName)->find();
                        if(!empty($shopAdmin)){
                            if($shopAdmin['shop_id'] != $admin_id){
                                $value = array('status'=>400,'mess'=>'登录账户已存在！','data'=>array('status'=>400));
                                return json($value);
                            }
                        }

                        $old_pwd = md5($old_password);
                        $admin_password = Db::name('shop_admin')->where('id',$admin_id)->value('password');
                        if($admin_password == $old_pwd){
                            if(!preg_match("/^[a-zA-Z0-9]{5,14}$/", $password)){
                                $value = array('status'=>400,'mess'=>'新密码为6-15位数字、英文、下划线组成','data'=>array('status'=>400));
                                return json($value);
                            }

                            if($password == $old_password){
                                $value = array('status'=>0,'mess'=>'新密码不能和旧密码相同');
                                return json($value);
                            }

                            if($confirm_password != $password){
                                $value = array('status'=>0,'mess'=>'确认密码不正确');
                                return json($value);
                            }

                            $count = Db::name('shop_admin')->update(array('password'=>md5($password),'id'=>$admin_id,'user_name'=>$userName));
                            if($count > 0){
                                $value = array('status'=>1,'mess'=>'修改登录密码成功');
                            }else{
                                $value = array('status'=>0,'mess'=>'修改登录密码失败');
                            }
                        }else{
                            $value = array('status'=>0,'mess'=>'旧密码错误');
                        }
                    }else{
                        $value = array('status'=>0,'mess'=>'确认密码不能为空');
                    }
                }else{
                    $value = array('status'=>0,'mess'=>'新密码不能为空');
                }
            }else{
                $value = array('status'=>0, 'mess'=>'旧密码不能为空');
            }
            return json($value);
        }else{
            $userName = db('shop_admin')->where('id',$admin_id)->value('user_name');
            $this->assign('user_name',$userName);
            return $this->fetch();
        }
    }

    //更改支付密码
    public function addpaypwd(){
        if(request()->isPost()) {
            $admin_id = session('shopadmin_id');
            $phone = Db::name('shop_admin')->where('id', $admin_id)->value('phone');
            if (input('post.phonecode')) {
                $code = input('post.phonecode');
                $smsCode = new SmsCode();
                $checkSmsCode = $smsCode->checkSmsCode($code, $phone, 5);
                if ($checkSmsCode['status'] == 1) {
                    $paypwd = input('post.paypwd');
                    if (!$paypwd || !preg_match("/^\\d{6}$/", $paypwd)) {
                        $value = array('status' => 0, 'mess' => '支付密码只能为6位数字组成');
                        return json($value);
                    }

                    $confirm_pwd = input('post.confirm_pwd');
                    if (!$confirm_pwd || $confirm_pwd != $paypwd) {
                        $value = array('status' => 0, 'mess' => '确认密码不正确');
                        return json($value);
                    }
                    // 启动事务
                    Db::startTrans();
                    try {
                        Db::name('shop_admin')->update(array('id' => $admin_id, 'paypwd' => md5($paypwd)));
                        // 提交事务
                        Db::commit();
                        $value = array('status' => 1, 'mess' => '设置支付密码成功');
                    } catch (\Exception $e) {
                        // 回滚事务
                        Db::rollback();
                        $value = array('status' => 0, 'mess' => '设置支付密码失败');
                    }
                } else {
                    $value = array('status' => 0, 'mess' => $checkSmsCode['mess']);
                }
            } else {
                $value = array('status' => 0, 'mess' => '请填写手机验证码');
            }
            return json($value);
        }
        $admin_id = session('shopadmin_id');
        $phone = Db::name('shop_admin')->where('id', $admin_id)->value('phone');
        $peizhi = Db::name('config')->where('ename', 'messtime')->field('value')->find();
        $this->assign('messtime', $peizhi['value']);
        $this->assign('phone', $phone);
        return $this->fetch();
    }

    //更手机号码
    public function editphone(){
        if(request()->isPost()){
            $admin_id = session('shopadmin_id');
            if(input('post.phone') && preg_match("/^1[3456789]{1}\\d{9}$/", input('post.phone'))){
                $phone = input('post.phone');
                $infos = Db::name('shop_admin')->where('phone',$phone)->field('id')->find();
                if(!$infos){
                    $adminphone = Db::name('shop_admin')->where('id',$admin_id)->value('phone');
                    if($adminphone != $phone){
                        if(input('post.phonecode')){
                            $code = input('post.phonecode');
                            $smsCodeModel = new SmsCodeModel();
                            $checkSmsCode = $smsCodeModel->checkSmsCode($code, $phone, 6);
                            if($checkSmsCode['status'] == 1){
                                $data = array();
                                $data['phone'] = $phone;
                                $data['id'] = $admin_id;
                                // 启动事务
                                Db::startTrans();
                                try{
                                    Db::name('shop_admin')->update($data);
                                    // 提交事务
                                    Db::commit();
                                    $value = array('status'=>1,'mess'=>'更换登录手机号成功');
                                } catch (\Exception $e) {
                                    // 回滚事务
                                    Db::rollback();
                                    $value = array('status'=>0,'mess'=>'更换登录手机号失败');
                                }
                            }else{
                                $value = array('status'=>0,'mess'=>$checkSmsCode['mess']);
                            }
                        }else{
                            $value = array('status'=>0, 'mess'=>'请填写手机验证码');
                        }
                    }else{
                        $value = array('status'=>0, 'mess'=>'新手机号与旧手机号不能相同');
                    }                    
                }else{
                    $value = array('status'=>0, 'mess'=>'手机号已存在');
                }
            }else{
                $value = array('status'=>0, 'mess'=>'请填写正确的手机号码');
            }
            return json($value);
        }else{
            $admin_id = session('shopadmin_id');
            $peizhi = Db::name('config')->where('ename','messtime')->field('value')->find();
            $this->assign('messtime',$peizhi['value']);
            return $this->fetch();
        }        
    }
}