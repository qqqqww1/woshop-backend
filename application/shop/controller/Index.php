<?php
/*
 * @Descripttion:
 * @Copyright: 武汉一一零七科技有限公司©版权所有
 * @Contact: QQ:2487937004
 * @Date: 2020-03-10 02:15:07
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2020-08-03 16:14:50
 */
namespace app\shop\controller;
use app\shop\controller\Common;
use think\Db;

class Index extends Common
{
    public function index(){
        $webconfig = $this->webconfig;
        $shopMember = Db::name('member')->where('shop_id',session('shopsh_id'))->field('id')->find();
        $this->assign('webconfig',$webconfig);
        $this->assign('shopMember',$shopMember);
        $plugins=db('plugin')->where(['status'=>1,'name'=>'through','isclose'=>1])->find();
        if($plugins){
            $this->assign('through',1);
        }

        return $this->fetch();
    }

    public function index_v3(){
        $shop_id = session('shopsh_id');
        $wallets = Db::name('shop_wallet')->where('shop_id',$shop_id)->find();
        $ordercount = Db::name('order')->where('shop_id',$shop_id)->count();

        $nowtime = time();
        $year = date('Y',time());
        $year2 = $year+1;
        $month = date('m',time());
        $month2 = $month+1;

        $day = date('d',time());
        $nowmonth = strtotime($year.'-'.$month.'-01 00:00:00');
        $lastmonth = strtotime($year.'-'.$month2.'-01 00:00:00');
        $nowyear = strtotime($year.'-01-01 00:00:00');
        $lastyear = strtotime($year2.'-01-01 00:00:00');

        // 全部已支付订单
        $order_num = Db::name('order')->where('shop_id',$shop_id)->where('state',1)->count();
        // 本月所有订单
        $month_order_num_all = Db::name('order')->where('shop_id',$shop_id)->whereTime('addtime','m')->count();
        // 本月已支付订单
        $month_order_num = Db::name('order')->where('shop_id',$shop_id)->where('state',1)->whereTime('addtime','m')->count();
        // 本月已成交订单
        $deal_num = Db::name('order')->where('shop_id',$shop_id)->whereTime('addtime','m')->where('order_status',1)->count();
        // 本月待支付订单
        $dai_num = Db::name('order')->where('shop_id',$shop_id)->whereTime('addtime','m')->where('state',0)->count();
        // 本月售后订单
        $shou_num = Db::name('order')->where('shop_id',$shop_id)->whereTime('addtime','m')->where('shouhou',1)->count();

        if($month_order_num_all > 0){
            $deal_lv = sprintf("%.2f",$deal_num/$month_order_num_all)*100;
            $dai_lv = sprintf("%.2f",$dai_num/$month_order_num_all)*100;
            $shou_lv = sprintf("%.2f",$shou_num/$month_order_num_all)*100;
        }else{
            $deal_lv = 0;
            $dai_lv = 0;
            $shou_lv = 0;
        }

        $month_salenum = Db::name('order_goods')->alias('a')->join('sp_order b','a.order_id=b.id','INNER')->where('b.shop_id',$shop_id)->where('b.state',1)->where('b.addtime','egt',$nowmonth)->where('b.addtime','lt',$lastyear)->count();
        $month_tuinum = Db::name('order_goods')->alias('a')->join('sp_order b','a.order_id = b.id','INNER')->where('b.shop_id',$shop_id)->where('a.th_status','in','1,2,3,4,9')->where('b.state',1)->where('b.addtime','egt',$nowmonth)->where('b.addtime','lt',$lastyear)->count();
        $month_huannum = Db::name('order_goods')->alias('a')->join('sp_order b','a.order_id = b.id','INNER')->where('b.shop_id',$shop_id)->where('a.th_status','in','5,6,7,8')->where('b.state',1)->where('b.addtime','egt',$nowmonth)->where('b.addtime','lt',$lastyear)->count();

        $monthSalenumStr = '';
        $monthTuinumStr = '';
        $monthHuannumStr = '';
        // 全年各月销售量、退款量、换货量
        for ($i=1; $i <= 12; $i++) {
            $monthSalenum = Db::name('order_goods')->alias('a')->join('sp_order b','a.order_id=b.id','INNER')->where('b.shop_id',$shop_id)->where('b.state',1)->where("FROM_UNIXTIME(b.addtime,'%m') = ".$i)->where('b.addtime','>',$nowyear)->count();
            $monthTuinum = Db::name('order_goods')->alias('a')->join('sp_order b','a.order_id = b.id','INNER')->where('b.shop_id',$shop_id)->where('a.th_status','in','1,2,3,4,9')->where('b.state',1)->where("FROM_UNIXTIME(b.addtime,'%m') = ".$i)->where('b.addtime','>',$nowyear)->count();
            $monthHuannum = Db::name('order_goods')->alias('a')->join('sp_order b','a.order_id = b.id','INNER')->where('b.shop_id',$shop_id)->where('a.th_status','in','5,6,7,8')->where('b.state',1)->where("FROM_UNIXTIME(b.addtime,'%m') = ".$i)->where('b.addtime','>',$nowyear)->count();
            $i < 12 ? $separator = ',' : $separator = '';
            $monthSalenumStr .= $monthSalenum.$separator;
            $monthTuinumStr .= $monthTuinum.$separator;
            $monthHuannumStr .= $monthHuannum.$separator;
        }

        // 总营业额
        $totalTurnover = Db::name('order')->where('state','1')->where('shop_id',$shop_id)->sum('goods_price');
        //访客人数
        $memberNum = db('shops')->where('id',$shop_id)->value('shop_visitor');

        //判断平台是否安装了 商户等级插件
        $is_plugin = ["is_shoplevel_plugin"=>0,"shoplevel_name"=>"默认等级"];
        $shoplevel_plugin = Db::name("plugin")->where(["name"=>"shoplevel","status"=>1,"isclose"=>1])->find();
        if($shoplevel_plugin){
            $is_plugin['is_shoplevel_plugin'] = 1;
            $shoplevel_name = Db::name("plugin_shoplevel_user")->where(["shop_id"=>$shop_id,"status"=>1])->order("shoplevel_id","DESC")->limit(1)->value("level_name");
            $is_plugin['shoplevel_name'] = $shoplevel_name ? $shoplevel_name : "默认等级";
        }
        $this->assign("plugin",$is_plugin);

        //判断平台是否安装了直通车插件
        $plugins=db('plugin')->where(['status'=>1,'name'=>'through','isclose'=>1])->find();
        if($plugins){
            $throughOrderList = Db::name('plugin_through_order')->where(['shop_id'=>$shop_id,'type'=>1])->select();
            foreach ($throughOrderList as $k => $v){
                $remainNumber = $v['number'] - $v['uses_number'];
                if($remainNumber < 1){
                    unset($throughOrderList[$k]);
                }else{
                    $throughOrderList[$k]['remain_number'] = $remainNumber;
                }
            }
            $throughShop = Db::name('plugin_through_shop')->where(['shop_id'=>$shop_id,'state'=>1])->find();
            if($throughShop){
                $this->assign('throughShop',1);
                $this->assign('through_order_id',$throughShop['through_order_id']);
            }
            if($plugins){
                $this->assign('through',1);
                $this->assign('through_order_list',$throughOrderList);
            }
        }


        $this->assign('memberNum',$memberNum);
        $this->assign('order_num',$order_num);
        $this->assign('month_order_num',$month_order_num);
        $this->assign('wallet_price',$wallets['price']);
        $this->assign('ordercount',$ordercount);
        $this->assign('deal_lv',$deal_lv);
        $this->assign('dai_lv',$dai_lv);
        $this->assign('shou_lv',$shou_lv);
        $this->assign('month_salenum',$month_salenum);
        $this->assign('month_tuinum',$month_tuinum);
        $this->assign('month_huannum',$month_huannum);
        $this->assign('totalTurnover',$totalTurnover);
        $this->assign('month',date('n',time()));
        $this->assign('year', $year);
        $this->assign('monthSalenumStr',$monthSalenumStr);
        $this->assign('monthTuinumStr',$monthTuinumStr);
        $this->assign('monthHuannumStr',$monthHuannumStr);
        return $this->fetch();
    }


    /**
     * 当前等级说明
     */
    public function index_shoplevel(){
        $shop_id = session('shopsh_id');
        $list = Db::name("plugin_shoplevel")->select();
        foreach ($list as $key=>$value){
            $user_shoplevel_id = Db::name("plugin_shoplevel_user")->where(["shop_id"=>$shop_id,"status"=>1])->order("shoplevel_id","DESC")->value("shoplevel_id");
            if($value['id'] == $user_shoplevel_id){
                $list[$key]['status'] = 1;
            }else{
                $list[$key]['status'] = 0;
            }

        }
        $this->assign("list",$list);
        return $this->fetch("index_shoplevel");
    }


    /**
     * 用户充值等级信息
     */
    public function index_shoplevel_submit(){
        $shop_id = session('shopsh_id');
        if(Request()->isAjax()){
            $params = input();
            if(!isset($params['shoplevel_id']) || empty($params['shoplevel_id'])){
                return json(['status'=>false,'msg'=>'请选择充值等级']);
            }
            if(!isset($params['voucher_img']) || empty($params['voucher_img'])){
                return json(['status'=>false,'msg'=>'请上传支付凭证']);
            }
            $shoplevel = Db::name("plugin_shoplevel")->where("id",$params['shoplevel_id'])->find();
            if(empty($shoplevel)){
                return json(['status'=>false,'msg'=>'没有找到等级信息']);
            }
            $is_shoplevel_user = Db::name("plugin_shoplevel_user")->where(["shop_id"=>$shop_id,"admin_check"=>1])->count();
            if($is_shoplevel_user){
                return json(['status'=>false,'msg'=>"您已经申请等级了，等待后台管理员审核"]);
            }
            $member_user_id = Db::name("member")->where("shop_id",$shop_id)->value("id");
            $user_id = $member_user_id ? $member_user_id : 0;

            $user_shoplevel_has = Db::name("plugin_shoplevel_user")->where(["shop_id"=>$shop_id,"shoplevel_id"=>$params['shoplevel_id']])->find();
            if($user_shoplevel_has['status'] == 1){
                return json(['status'=>false,'msg'=>"您已经升级过当前等级"]);
            }
            if($user_shoplevel_has){
                $user_shoplevel_has['user_id'] = $user_id;
                $user_shoplevel_has['shop_id'] = $shop_id;
                $user_shoplevel_has['level_name'] = $shoplevel['level_name'];
                $user_shoplevel_has['status'] = 0;
                $user_shoplevel_has['updated_at'] = time();
                $user_shoplevel_has['voucher_img'] = $params['voucher_img'];
                $user_shoplevel_has['bond'] = $shoplevel['bond'];
                $user_shoplevel_has['rate'] = $shoplevel['rate'];
                $user_shoplevel_has['admin_check'] = 1;
                $result = Db::name("plugin_shoplevel_user")->update($user_shoplevel_has);
            }else{
                $data['shoplevel_id'] = $params['shoplevel_id'];
                $data['user_id'] = $user_id;
                $data['shop_id'] = $shop_id;
                $data['level_name'] = $shoplevel['level_name'];
                $data['status'] = 0;
                $data['created_at'] = time();
                $data['updated_at'] = time();
                $data['voucher_img'] = $params['voucher_img'];
                $data['bond'] = $shoplevel['bond'];
                $data['rate'] = $shoplevel['rate'];
                $data['admin_check'] = 1;
                $result = Db::name("plugin_shoplevel_user")->insertGetId($data);
            }
            if($result){
                return json(['status'=>true,'msg'=>'申请成功']);
            }else{
                return json(['status'=>false,'msg'=>'申请失败']);
            }

        }else{
            $config = Db::name("plugin_shoplevel_config")->select();
            $list = Db::name("plugin_shoplevel")->select();
            $shoukuan = Db::name("plugin_shoplevel_config")->where("enname","pay_code_img")->value("value");
            $this->assign("shoukuan",$shoukuan);
            $this->assign("config",$config);
            $this->assign("list",$list);
            return $this->fetch("index_shoplevel_submit");
        }
    }


    /**
     * 根据等级信息获取等级
     */
    public function shop_level_name(){
        $params = input();
        $shoplevel = Db::name("plugin_shoplevel")->where("id",$params['id'])->find();
        return json(['status'=>true,'data'=>$shoplevel]);
    }

}