<?php
namespace app\api\controller;
use app\api\controller\Common;
use think\Db;
use app\api\model\Currency as CurrencyModel;
use app\api\controller\Location as location;
class Currency extends Common
{
    //获取货币列表
    public function getCurrencyList(){
        $tokenRes = $this->checkToken(0);
        if($tokenRes['status'] == 400){
            datamsg(400,$tokenRes['mess'],$tokenRes['data']);
        }
        $plugins=db('plugin')->where('name','Currency')->find();
        if($plugins){
            $currencyModel = new CurrencyModel();
            $currencyLst=$currencyModel->select();
            if($currencyLst){
                datamsg(200,'获取货币列表成功',$currencyLst);
            }else{
                datamsg(400,'获取货币列表失败',array('status'=>400));
            }
        }else{
            datamsg(200,'暂无插件汇率插件',[]);
        }
    }
}
