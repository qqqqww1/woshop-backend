<?php
/*
 * @Descripttion:
 * @Copyright: 武汉一一零七科技有限公司©版权所有
 * @Contact: QQ:2487937004
 * @Date: 2020-03-10 02:15:07
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2020-08-03 16:14:50
 */

namespace app\api\controller;

use app\api\controller\Common;
use think\Db;

class MerchantNew extends Common
{
    public function overview()
    {
        $tokenRes = $this->checkToken();
        if ($tokenRes['status'] == 400) {
            datamsg(400, $tokenRes['mess'], $tokenRes['data']);
        } else {
            $user_id = $tokenRes['user_id'];
        }

        $members = db('member')->where(['id' => $user_id])->find();
        $shopId = $members['shop_id'];

        $wallets = Db::name('shop_wallet')->where('shop_id', $shopId)->find();
        $ordercount = Db::name('order')->where('shop_id', $shopId)->count();

        $nowtime = time();
        $year = date('Y', time());
        $year2 = $year + 1;
        $month = date('m', time());
        $month2 = $month + 1;

        $day = date('d', time());
        $nowmonth = strtotime($year . '-' . $month . '-01 00:00:00');
        $lastmonth = strtotime($year . '-' . $month2 . '-01 00:00:00');
        $nowyear = strtotime($year . '-01-01 00:00:00');
        $lastyear = strtotime($year2 . '-01-01 00:00:00');

        // 全部已支付订单
        $order_num = Db::name('order')->where('shop_id', $shopId)->where('state', 1)->count();
        // 本月所有订单
        $month_order_num_all = Db::name('order')->where('shop_id', $shopId)->whereTime('addtime', 'm')->count();
        // 本月已支付订单
        $month_order_num = Db::name('order')->where('shop_id', $shopId)->where('state', 1)->whereTime('addtime', 'm')->count();
        // 本月已成交订单
        $deal_num = Db::name('order')->where('shop_id', $shopId)->whereTime('addtime', 'm')->where('order_status', 1)->count();
        // 本月待支付订单
        $dai_num = Db::name('order')->where('shop_id', $shopId)->whereTime('addtime', 'm')->where('state', 0)->count();
        // 本月售后订单
        $shou_num = Db::name('order')->where('shop_id', $shopId)->whereTime('addtime', 'm')->where('shouhou', 1)->count();

        if ($month_order_num_all > 0) {
            $deal_lv = sprintf("%.2f", $deal_num / $month_order_num_all) * 100;
            $dai_lv = sprintf("%.2f", $dai_num / $month_order_num_all) * 100;
            $shou_lv = sprintf("%.2f", $shou_num / $month_order_num_all) * 100;
        } else {
            $deal_lv = 0;
            $dai_lv = 0;
            $shou_lv = 0;
        }

        $month_salenum = Db::name('order_goods')->alias('a')->join('sp_order b', 'a.order_id=b.id', 'INNER')->where('b.shop_id', $shopId)->where('b.state', 1)->where('b.addtime', 'egt', $nowmonth)->where('b.addtime', 'lt', $lastyear)->count();
        $month_tuinum = Db::name('order_goods')->alias('a')->join('sp_order b', 'a.order_id = b.id', 'INNER')->where('b.shop_id', $shopId)->where('a.th_status', 'in', '1,2,3,4')->where('b.state', 1)->where('b.addtime', 'egt', $nowmonth)->where('b.addtime', 'lt', $lastyear)->count();
        $month_huannum = Db::name('order_goods')->alias('a')->join('sp_order b', 'a.order_id = b.id', 'INNER')->where('b.shop_id', $shopId)->where('a.th_status', 'in', '5,6,7,8')->where('b.state', 1)->where('b.addtime', 'egt', $nowmonth)->where('b.addtime', 'lt', $lastyear)->count();

        $monthSalenumStr = '';
        $monthTuinumStr = '';
        $monthHuannumStr = '';
        // 全年各月销售量、退款量、换货量
        for ($i = 1; $i <= 12; $i++) {
            $monthSalenum = Db::name('order_goods')->alias('a')->join('sp_order b', 'a.order_id=b.id', 'INNER')->where('b.shop_id', $shopId)->where('b.state', 1)->where("FROM_UNIXTIME(b.addtime,'%m') = " . $i)->where('b.addtime', '>', $nowyear)->count();
            $monthTuinum = Db::name('order_goods')->alias('a')->join('sp_order b', 'a.order_id = b.id', 'INNER')->where('b.shop_id', $shopId)->where('a.th_status', 'in', '1,2,3,4')->where('b.state', 1)->where("FROM_UNIXTIME(b.addtime,'%m') = " . $i)->where('b.addtime', '>', $nowyear)->count();
            $monthHuannum = Db::name('order_goods')->alias('a')->join('sp_order b', 'a.order_id = b.id', 'INNER')->where('b.shop_id', $shopId)->where('a.th_status', 'in', '5,6,7,8')->where('b.state', 1)->where("FROM_UNIXTIME(b.addtime,'%m') = " . $i)->where('b.addtime', '>', $nowyear)->count();
            $i < 12 ? $separator = ',' : $separator = '';
            $monthSalenumStr .= $monthSalenum . $separator;
            $monthTuinumStr .= $monthTuinum . $separator;
            $monthHuannumStr .= $monthHuannum . $separator;
        }

        // 总营业额
        $totalTurnover = Db::name('order')->where('state', '1')->where('shop_id', $shopId)->sum('goods_price');
        // 当月营业额
        $monthTurnover = Db::name('order')->where('state', '1')->where('shop_id', $shopId)->whereTime('addtime', 'm')->sum('goods_price');
        // 进货金额
        $purchase = Db::name('order')->where('shop_id', $shopId)->where('state', 1)->sum('shop_price');
        // 在途资金
        $onway = Db::name('order')->where('shop_id', $shopId)->where(array('state' => 1, 'fh_status' => 1, 'order_status' => 0, 'is_show' => 1))->sum('shop_price');
        // 访客数
        $visitor = Db::name('shops')->where('id', $shopId)->find('shop_visitor');

        $data = [];
        $data['order_num'] = $order_num;
        $data['month_order_num'] = $month_order_num;
        $data['wallet_price'] = $wallets['price'];
        $data['ordercount'] = $ordercount;
        $data['deal_lv'] = $deal_lv;
        $data['dai_lv'] = $dai_lv;
        $data['shou_lv'] = $shou_lv;
        $data['month_salenum'] = $month_salenum;
        $data['month_tuinum'] = $month_tuinum;
        $data['month_huannum'] = $month_huannum;
        $data['monthTurnover'] = $monthTurnover;
        $data['purchase'] = $purchase;
        $data['onway'] = $onway;
        $data['totalTurnover'] = $totalTurnover;
        $data['visitor'] = $visitor;
        $data['month'] = date('n', time());
        $data['year'] = $year;
        $data['monthSalenumStr'] = $monthSalenumStr;
        $data['monthTuinumStr'] = $monthTuinumStr;
        $data['monthHuannumStr'] = $monthHuannumStr;
        datamsg(200, '获取店铺主页信息成功', $data);
    }

    public function shoplevel()
    {
        $tokenRes = $this->checkToken();
        if ($tokenRes['status'] == 400) {
            datamsg(400, $tokenRes['mess'], $tokenRes['data']);
        } else {
            $user_id = $tokenRes['user_id'];
        }

        $members = db('member')->where(['id' => $user_id])->find();
        $shopId = $members['shop_id'];

        $level = Db::name('plugin_shoplevel_user')->where('shop_id', $shopId)->find();
        if (!$level) {
            $level['level'] = '普通会员';
            $level['rate'] = 20;
        }

        datamsg(200, '获取店铺等级信息成功', $level);
    }
}
