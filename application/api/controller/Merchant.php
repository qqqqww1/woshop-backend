<?php

namespace app\api\controller;

use app\api\controller\Common;
use think\db;
use app\api\model\Common as CommonModel;
use app\api\model\Member;
use app\common\model\SmsCode;
use app\shop\model\Goods as GoodsModel;
use app\shop\model\GoodsSpec as GoodsSpecModel;
use app\shop\model\GoodsSpecItem as GoodsSpecItemModel;
use app\shop\model\GoodsPic as GoodsPicModel;
use app\shop\model\GoodsAttr as GoodsAttrModel;
use app\shop\model\GoodsParam as GoodsParamModel;
use app\shop\model\GoodsOption as GoodsOptionModel;
use app\shop\model\GoodsLang as GoodsLangModel;

class Merchant extends Common
{
    public function bindSubAccount()
    {
        $tokenRes = $this->checkToken();
        if ($tokenRes['status'] == 400) {
            datamsg(400, $tokenRes['mess'], $tokenRes['data']);
        } else {
            $userId = $tokenRes['user_id'];
        }
        $userModel = new Member();
        $user = $userModel->getUserInfoById($userId);
        if (empty($user['shop_id'])) {
            datamsg(400, '您没有添加子账号权限！');
        }
        $data['phone'] = input('param.phone');
        $data['sms_code'] = input('param.sms_code');
        $validate = validate('AddSubAccount');
        if (!$validate->check($data)) {
            datamsg(400, $validate->getError());
        }
        $subUser = $userModel->getUserInfoByPhone($data['phone']);
        if (!$subUser) {
            datamsg(400, '该手机号未注册，请先注册账号再进行绑定！');
        }
        if ($subUser['id'] == $userId) {
            datamsg(400, '子账号不能添加自己！');
        }
        $smsCodeModel = new SmsCode();
        $checkSmsCode = $smsCodeModel->checkSmsCode($data['sms_code'], $data['phone'], 4);
        if ($checkSmsCode['status'] == 400) {
            datamsg(400, $checkSmsCode['mess']);
        }

        $bindParent = Db::name('member')->where(['id' => $subUser['id']])->update(['pid' => $userId]);
        if ($bindParent) {
            datamsg(200, '绑定成功');
        } else {
            datamsg(400, '绑定失败');
        }
    }

    // 客服列表
    public function customerServiceList()
    {
        $tokenRes = $this->checkToken();
        if ($tokenRes['status'] == 400) {
            datamsg(400, $tokenRes['mess'], $tokenRes['data']);
        } else {
            $userId = $tokenRes['user_id'];
        }

        $customerServiceList = db('member')->where(['pid' => $userId])->select();
        foreach ($customerServiceList as $k => $v) {
            $customerServiceToken = db('member_token')->where(['user_id' => $v['id']])->value('token');
            if ($customerServiceToken) {
                $v['toid'] = $customerServiceToken;
                $v['headimgurl'] = url_format($v['headimgurl'], $this->webconfig['weburl']);
                $customerServiceListRes[] = $v;
            }
        }
        datamsg(200, '获取成功', $customerServiceListRes);
    }


    // 停用子账号
    public function deleteCustomerService()
    {
        $tokenRes = $this->checkToken();
        if ($tokenRes['status'] == 400) { // 400返回错误描述
            datamsg(400, $tokenRes['mess'], $tokenRes['data']);
        }
        $id = input('post.id');
        if (empty($id)) {
            datamsg(400, '缺少子账号ID参数');
        }

        $res = Db::name('member')->where(['id' => $id])->update(['pid' => 0]);
        if ($res) {
            datamsg(200, '解绑成功');
        } else {
            datamsg(400, '解绑失败');
        }
    }

    // 订单列表
    //订单列表信息接口
    public function orderList()
    {
        $tokenRes = $this->checkToken();
        if ($tokenRes['status'] == 400) {
            datamsg(400, $tokenRes['mess'], $tokenRes['data']);
        } else {
            $userId = $tokenRes['user_id'];
        }

        $members = db('member')->where(['id' => $userId])->find();
        $shop_id = $members['shop_id'];

        if (empty($shop_id) || $shop_id == 0) {
            //查看是否是客服登录进来， 如果是客服，商家id 等于客服的父级的商家id
            if ($members['pid'] > 0) {
                $shop_id = db('member')->where(['id' => $members['pid']])->value('shop_id');
            }
        }

        if (empty($shop_id) || $shop_id == 0) {
            datamsg(400, '缺少商家id参数');
        }
        if (!input('post.page') || !preg_match("/^\\+?[1-9][0-9]*$/", input('post.page'))) {
            datamsg(400, '缺少页数参数');
        }
        $ordouts = Db::name('order_timeout')->where('id', 1)->find();
        $webconfig = $this->webconfig;
        $perpage = 20;
        $offset = (input('post.page') - 1) * $perpage;

        $filter = input('post.filter');
        if (!$filter || !in_array($filter, array(1, 2, 3, 4, 5, 6, 7, 8, 9))) {
            $filter = 6;
        }
        switch ($filter) {
                //待付款
            case 1:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 0, 'a.fh_status' => 0, 'a.order_status' => 0, 'a.is_show' => 1);
                $sort = array('a.addtime' => 'desc', 'a.id' => 'desc');
                break;
                //待发货
            case 2:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 1, 'a.fh_status' => 0, 'a.order_status' => 0, 'a.is_show' => 1);
                $sort = array('a.pay_time' => 'desc', 'a.id' => 'desc');
                break;
                //待收货
            case 3:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 1, 'a.fh_status' => 1, 'a.order_status' => 0, 'a.is_show' => 1);
                $sort = array('a.fh_time' => 'desc', 'a.id' => 'desc');
                break;
                //待评价
            case 4:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 1, 'a.fh_status' => 1, 'a.order_status' => 1, 'a.ping' => 0, 'a.is_show' => 1);
                $sort = array('a.addtime' => 'desc', 'a.id' => 'desc');
                break;
                //待付款
            case 5:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 0, 'a.fh_status' => 0, 'a.order_status' => 0, 'a.is_show' => 1);
                $sort = array('a.addtime' => 'desc', 'a.id' => 'desc');
                break;
                //失败
            case 7:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 0, 'a.order_status' => 2);
                $sort = array('a.coll_time' => 'desc', 'a.id' => 'desc');
                break;
                //全部
                //已评价
            case 8:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 1, 'a.fh_status' => 1, 'a.order_status' => 1, 'a.ping' => 1, 'a.is_show' => 1);
                $sort = array('a.addtime' => 'desc', 'a.id' => 'desc');
                break;
            case 9:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 1, 'a.fh_status' => 1, 'a.order_status' => 1, 'a.is_show' => 1);
                $sort = array('a.addtime' => 'desc', 'a.id' => 'desc');
                break;
                //全部
            case 6:
                $where = array('a.shop_id' => $shop_id, 'a.is_show' => 1);
                $sort = array('a.addtime' => 'desc', 'a.id' => 'desc');
                break;
        }

        if (in_array($filter, array(1, 2, 3, 4, 5, 6, 7, 8, 9))) {
            $orderes = Db::name('order')
                ->alias('a')
                ->field('a.id,a.ordernumber,a.coupon_id,a.total_price,a.state,a.fh_status,a.order_status,a.shouhou,a.ping,a.is_show,a.ping,a.order_type,a.pin_type,a.pin_id,a.shop_id,a.zdsh_time,a.time_out,b.shop_name')
                ->join('sp_shops b', 'a.shop_id = b.id', 'INNER')
                ->where($where)
                ->order($sort)
                ->limit($offset, $perpage)
                ->select();

            if ($orderes) {
                foreach ($orderes as $k => $v) {
                    if ($v['state'] == 0 && $v['fh_status'] == 0 && $v['order_status'] == 0 && $v['is_show'] == 1) {
                        $orderes[$k]['order_zt'] = "待付款";
                        $orderes[$k]['filter'] = 1;
                    } elseif ($v['state'] == 1 && $v['fh_status'] == 0 && $v['order_status'] == 0 && $v['is_show'] == 1) {
                        $orderes[$k]['order_zt'] = "待发货";
                        $orderes[$k]['filter'] = 2;
                    } elseif ($v['state'] == 1 && $v['fh_status'] == 1 && $v['order_status'] == 0 && $v['is_show'] == 1) {
                        $orderes[$k]['order_zt'] = "待收货";
                        $orderes[$k]['filter'] = 3;
                    } elseif ($v['state'] == 1 && $v['fh_status'] == 1 && $v['order_status'] == 1 && $v['is_show'] == 1) {
                        $orderes[$k]['order_zt'] = "已完成";
                        $orderes[$k]['filter'] = 4;
                    } elseif ($v['order_status'] == 2 && $v['is_show'] == 1) {
                        $orderes[$k]['order_zt'] = "已关闭";
                        $orderes[$k]['filter'] = 5;
                    }

                    $orderes[$k]['goodsinfo'] = Db::name('order_goods')
                        ->where('order_id', $v['id'])
                        ->field('id,goods_id,goods_name,thumb_url,goods_attr_str,price,goods_num,th_status,order_id')
                        ->find();
                    //print_r($orderes[$k]['goodsinfo']);

                    $orderes[$k]['goodsinfo']['thumb_url'] = url_format($orderes[$k]['goodsinfo']['thumb_url'], $webconfig['weburl'], '?imageMogr2/thumbnail/200x200');



                    $orderes[$k]['spnum'] = Db::name('order_goods')->where('order_id', $v['id'])->sum('goods_num');
                }
            }
        } else {

            $orderes = Db::name('th_apply')
                ->alias('a')
                ->field('a.id,a.th_number,a.thfw_id,a.apply_status,a.tui_price,a.tui_num,a.orgoods_id,a.order_id,a.dcfh_status,a.sh_status,a.fh_status,a.shou_status,a.check_timeout,a.shoptui_timeout,a.yhfh_timeout,a.yhshou_timeout,a.shop_id,b.shop_name')
                ->join('sp_shops b', 'a.shop_id = b.id', 'INNER')
                ->where('a.shop_id', $shop_id)
                ->order('a.apply_time desc')
                ->limit($offset, $perpage)
                ->select();
            if ($orderes) {
                foreach ($orderes as $k => $v) {
                    switch ($v['thfw_id']) {
                        case 1:
                            if ($v['apply_status'] == 0) {
                                $orderes[$k]['order_zt'] = '待商家处理';
                            } elseif ($v['apply_status'] == 1) {
                                $orderes[$k]['order_zt'] = '待商家退款';
                            } elseif ($v['apply_status'] == 2) {
                                $orderes[$k]['order_zt'] = '商家拒绝申请';
                            } elseif ($v['apply_status'] == 3) {
                                $orderes[$k]['order_zt'] = '退款已完成';
                            } elseif ($v['apply_status'] == 4) {
                                $orderes[$k]['order_zt'] = '已撤销';
                            }
                            break;
                        case 2:
                            if ($v['apply_status'] == 0) {
                                $orderes[$k]['order_zt'] = '待商家处理';
                            } elseif ($v['apply_status'] == 1) {
                                if ($v['dcfh_status'] == 0) {
                                    $orderes[$k]['order_zt'] = '待用户发货';
                                } elseif ($v['dcfh_status'] == 1 && $v['sh_status'] == 0) {
                                    $orderes[$k]['order_zt'] = '待商家收货';
                                } elseif ($v['dcfh_status'] == 1 && $v['sh_status'] == 1) {
                                    $orderes[$k]['order_zt'] = '待商家退款';
                                }
                            } elseif ($v['apply_status'] == 2) {
                                $orderes[$k]['order_zt'] = '商家拒绝申请';
                            } elseif ($v['apply_status'] == 3) {
                                $orderes[$k]['order_zt'] = '退款已完成';
                            } elseif ($v['apply_status'] == 4) {
                                $orderes[$k]['order_zt'] = '已撤销';
                            }
                            break;
                        case 3:
                            if ($v['apply_status'] == 0) {
                                $orderes[$k]['order_zt'] = '待商家处理';
                            } elseif ($v['apply_status'] == 1) {
                                if ($v['dcfh_status'] == 0) {
                                    $orderes[$k]['order_zt'] = '待用户发货';
                                } elseif ($v['dcfh_status'] == 1 && $v['sh_status'] == 0) {
                                    $orderes[$k]['order_zt'] = '待商家收货';
                                } elseif ($v['sh_status'] == 1 && $v['fh_status'] == 0) {
                                    $orderes[$k]['order_zt'] = '待商家发货';
                                } elseif ($v['fh_status'] == 1 && $v['shou_status'] == 0) {
                                    $orderes[$k]['order_zt'] = '待用户收货';
                                }
                            } elseif ($v['apply_status'] == 2) {
                                $orderes[$k]['order_zt'] = '商家拒绝申请';
                            } elseif ($v['apply_status'] == 3) {
                                $orderes[$k]['order_zt'] = '换货已完成';
                            } elseif ($v['apply_status'] == 4) {
                                $orderes[$k]['order_zt'] = '已撤销';
                            }
                            break;
                    }
                    $orderes[$k]['orgoods'] = Db::name('order_goods')
                        ->where('id', $v['orgoods_id'])
                        ->where('order_id', $v['order_id'])
                        ->field('id,goods_id,goods_name,thumb_url,goods_attr_str,goods_num,th_status,order_id')
                        ->find();
                    $orderes[$k]['orgoods']['thumb_url'] = url_format($orderes[$k]['orgoods']['thumb_url'], $webconfig['weburl'], '?imageMogr2/thumbnail/350x350');

                    if ($v['apply_status'] == 0 && $v['check_timeout'] <= time()) {
                        // 启动事务
                        Db::startTrans();
                        try {
                            if ($v['thfw_id'] == 1) {
                                $shoptui_timeout = time() + $ordouts['shoptui_timeout'] * 24 * 3600;
                                Db::name('th_apply')->update(array('apply_status' => 1, 'agree_time' => time(), 'shoptui_timeout' => $shoptui_timeout, 'id' => $v['id']));
                            } elseif (in_array($v['thfw_id'], array(2, 3))) {
                                $yhfh_timeout = time() + $ordouts['yhfh_timeout'] * 24 * 3600;
                                Db::name('th_apply')->update(array('apply_status' => 1, 'agree_time' => time(), 'yhfh_timeout' => $yhfh_timeout, 'id' => $v['id']));
                            }

                            if (in_array($v['thfw_id'], array(1, 2))) {
                                $th_status = 2;
                            } elseif ($v['thfw_id'] == 3) {
                                $th_status = 6;
                            }

                            if (!empty($th_status)) {
                                Db::name('order_goods')->update(array('th_status' => $th_status, 'id' => $v['orgoods_id']));
                            }

                            // 提交事务
                            Db::commit();
                        } catch (\Exception $e) {
                            // 回滚事务
                            Db::rollback();
                        }
                    } elseif ($v['thfw_id'] == 1 && $v['apply_status'] == 1 && $v['shoptui_timeout'] <= time()) {
                        $orgoods = Db::name('order_goods')
                            ->where('id', $v['orgoods_id'])
                            ->field('goods_id,goods_attr_id,hd_type,hd_id')
                            ->find();
                        if ($orgoods) {
                            // 启动事务
                            Db::startTrans();
                            try {
                                Db::name('th_apply')->update(array('apply_status' => 3, 'com_time' => time(), 'id' => $v['id']));
                                Db::name('order_goods')->update(array('th_status' => 4, 'id' => $v['orgoods_id']));
                                $ordergoods = Db::name('order_goods')
                                    ->where('id', 'neq', $v['orgoods_id'])
                                    ->where('order_id', $v['order_id'])
                                    ->where('th_status', 'in', '0,1,2,3,5,6,7,8')
                                    ->field('id')
                                    ->find();
                                if (!$ordergoods) {
                                    $orders = Db::name('order')->where('id', $v['order_id'])->find();
                                    if ($orders) {
                                        Db::name('order')->where('id', $v['order_id'])->update(array('order_status' => 2, 'shouhou' => 0, 'can_time' => time()));
                                        if ($orders['coupon_id']) {
                                            Db::name('member_coupon')
                                                ->where('user_id', $orders['user_id'])
                                                ->where('coupon_id', $orders['coupon_id'])
                                                ->where('is_sy', 1)
                                                ->where('shop_id', $orders['shop_id'])
                                                ->update(array('is_sy' => 0));
                                        }
                                    }
                                } else {
                                    $ordergoodres = Db::name('order_goods')
                                        ->where('id', 'neq', $v['orgoods_id'])
                                        ->where('order_id', $v['order_id'])
                                        ->where('th_status', 'in', '1,2,3,5,6,7')
                                        ->field('id')
                                        ->find();

                                    if ($ordergoodres) {
                                        $shouhou = 1;
                                    } else {
                                        $shouhou = 0;
                                    }

                                    if ($shouhou == 0) {
                                        $orders = Db::name('order')->where('id', $v['order_id'])->find();
                                        if ($orders) {
                                            $zdsh_time = time() + $ordouts['zdqr_sh_time'] * 24 * 3600;
                                            Db::name('order')->where('id', $v['order_id'])->update(array('shouhou' => 0, 'zdsh_time' => $zdsh_time));
                                        }
                                    }
                                }

                                if (in_array($orgoods['hd_type'], array(0, 2, 3))) {
                                    $prokc = Db::name('product')->where('goods_id', $orgoods['goods_id'])->where('goods_attr', $orgoods['goods_attr_id'])->find();
                                    if ($prokc) {
                                        Db::name('product')->where('goods_id', $orgoods['goods_id'])->where('goods_attr', $orgoods['goods_attr_id'])->setInc('goods_number', $v['tui_num']);
                                    }
                                } elseif ($orgoods['hd_type'] == 1) {
                                    $hdactivitys = Db::name('seckill')->where('id', $orgoods['hd_id'])->find();
                                    if ($hdactivitys) {
                                        Db::name('seckill')->where('id', $orgoods['hd_id'])->setInc('stock', $v['tui_num']);
                                        Db::name('seckill')->where('id', $orgoods['hd_id'])->setDec('sold', $v['tui_num']);
                                    }
                                }

                                // 提交事务
                                Db::commit();
                            } catch (\Exception $e) {
                                // 回滚事务
                                Db::rollback();
                            }
                        }
                    } elseif ($v['thfw_id'] == 2 && $v['apply_status'] == 1 && $v['dcfh_status'] == 1 && $v['sh_status'] == 1 && $v['shoptui_timeout'] <= time()) {
                        $orgoods = Db::name('order_goods')
                            ->where('id', $v['orgoods_id'])
                            ->field('goods_id,goods_attr_id,hd_type,hd_id')
                            ->find();
                        if ($orgoods) {
                            // 启动事务
                            Db::startTrans();
                            try {
                                Db::name('th_apply')->update(array('apply_status' => 3, 'com_time' => time(), 'id' => $v['id']));
                                Db::name('order_goods')->update(array('th_status' => 4, 'id' => $v['orgoods_id']));
                                $ordergoods = Db::name('order_goods')
                                    ->where('id', 'neq', $v['orgoods_id'])
                                    ->where('order_id', $v['order_id'])
                                    ->where('th_status', 'in', '0,1,2,3,5,6,7,8')
                                    ->field('id')
                                    ->find();
                                if (!$ordergoods) {
                                    $orders = Db::name('order')->where('id', $v['order_id'])->find();
                                    if ($orders) {
                                        Db::name('order')->where('id', $v['order_id'])->update(array('order_status' => 2, 'shouhou' => 0, 'can_time' => time()));
                                        if ($orders['coupon_id']) {
                                            Db::name('member_coupon')
                                                ->where('user_id', $orders['user_id'])
                                                ->where('coupon_id', $orders['coupon_id'])
                                                ->where('is_sy', 1)
                                                ->where('shop_id', $orders['shop_id'])
                                                ->update(array('is_sy' => 0));
                                        }
                                    }
                                } else {
                                    $ordergoodres = Db::name('order_goods')
                                        ->where('id', 'neq', $v['orgoods_id'])
                                        ->where('order_id', $v['order_id'])
                                        ->where('th_status', 'in', '1,2,3,5,6,7')
                                        ->field('id')
                                        ->find();

                                    if ($ordergoodres) {
                                        $shouhou = 1;
                                    } else {
                                        $shouhou = 0;
                                    }

                                    if ($shouhou == 0) {
                                        $orders = Db::name('order')->where('id', $v['order_id'])->find();
                                        if ($orders) {
                                            $zdsh_time = time() + $ordouts['zdqr_sh_time'] * 24 * 3600;
                                            Db::name('order')->where('id', $v['order_id'])->update(array('shouhou' => 0, 'zdsh_time' => $zdsh_time));
                                        }
                                    }
                                }

                                if (in_array($orgoods['hd_type'], array(0, 2, 3))) {
                                    $prokc = Db::name('product')
                                        ->where('goods_id', $orgoods['goods_id'])
                                        ->where('goods_attr', $orgoods['goods_attr_id'])
                                        ->find();
                                    if ($prokc) {
                                        Db::name('product')
                                            ->where('goods_id', $orgoods['goods_id'])
                                            ->where('goods_attr', $orgoods['goods_attr_id'])
                                            ->setInc('goods_number', $v['tui_num']);
                                    }
                                } elseif ($orgoods['hd_type'] == 1) {
                                    $hdactivitys = Db::name('seckill')->where('id', $orgoods['hd_id'])->find();
                                    if ($hdactivitys) {
                                        Db::name('seckill')->where('id', $orgoods['hd_id'])->setInc('stock', $v['tui_num']);
                                        Db::name('seckill')->where('id', $orgoods['hd_id'])->setDec('sold', $v['tui_num']);
                                    }
                                }

                                // 提交事务
                                Db::commit();
                            } catch (\Exception $e) {
                                // 回滚事务
                                Db::rollback();
                            }
                        }
                    } elseif (in_array($v['thfw_id'], array(2, 3)) && $v['apply_status'] == 1 && $v['dcfh_status'] == 0 && $v['yhfh_timeout'] <= time()) {
                        $orders = Db::name('order')
                            ->where('id', $v['order_id'])
                            ->where('state', 1)
                            ->where('fh_status', 1)
                            ->field('id')
                            ->find();
                        if ($orders) {
                            // 启动事务
                            Db::startTrans();
                            try {
                                Db::name('th_apply')->update(array('apply_status' => 4, 'che_time' => time(), 'id' => $v['id']));
                                Db::name('order_goods')->update(array('th_status' => 0, 'id' => $v['orgoods_id']));

                                $ordergoods = Db::name('order_goods')
                                    ->where('id', 'neq', $v['orgoods_id'])
                                    ->where('order_id', $v['order_id'])
                                    ->where('th_status', 'in', '1,2,3,5,6,7')
                                    ->field('id')
                                    ->find();

                                if ($ordergoods) {
                                    $shouhou = 1;
                                } else {
                                    $shouhou = 0;
                                }

                                if ($shouhou == 0) {
                                    $orders = Db::name('order')->where('id', $v['order_id'])->find();
                                    if ($orders) {
                                        $zdsh_time = time() + $ordouts['zdqr_sh_time'] * 24 * 3600;
                                        Db::name('order')->where('id', $v['order_id'])->update(array('shouhou' => 0, 'zdsh_time' => $zdsh_time));
                                    }
                                }

                                // 提交事务
                                Db::commit();
                            } catch (\Exception $e) {
                                // 回滚事务
                                Db::rollback();
                            }
                        }
                    } elseif ($v['thfw_id'] == 3 && $v['apply_status'] == 1 && $v['dcfh_status'] == 1 && $v['sh_status'] == 1 && $v['fh_status'] == 1 && $v['shou_status'] == 0 && $v['yhshou_timeout'] <= time()) {
                        // 启动事务
                        Db::startTrans();
                        try {
                            Db::name('th_apply')->update(array('shou_status' => 1, 'apply_status' => 3, 'shou_time' => time(), 'com_time' => time(), 'id' => $v['id']));
                            Db::name('order_goods')->update(array('th_status' => 8, 'id' => $v['orgoods_id']));

                            $ordergoods = Db::name('order_goods')
                                ->where('id', 'neq', $v['orgoods_id'])
                                ->where('order_id', $v['order_id'])
                                ->where('th_status', 'in', '1,2,3,5,6,7')
                                ->field('id,th_status')
                                ->find();

                            if ($ordergoods) {
                                $shouhou = 1;
                            } else {
                                $shouhou = 0;
                            }

                            if ($shouhou == 0) {
                                $orders = Db::name('order')->where('id', $v['order_id'])->find();
                                if ($orders) {
                                    $zdsh_time = time() + $ordouts['zdqr_sh_time'] * 24 * 3600;
                                    Db::name('order')->where('id', $v['order_id'])->update(array('shouhou' => 0, 'zdsh_time' => $zdsh_time));
                                }
                            }

                            // 提交事务
                            Db::commit();
                        } catch (\Exception $e) {
                            // 回滚事务
                            Db::rollback();
                        }
                    }
                }
            }
        }
        datamsg(200, '获取订单信息成功', set_lang($orderes));
    }

    //订单列表信息接口
    public function orderCount()
    {
        $tokenRes = $this->checkToken();
        if ($tokenRes['status'] == 400) {
            datamsg(400, $tokenRes['mess'], $tokenRes['data']);
        } else {
            $userId = $tokenRes['user_id'];
        }

        $members = db('member')->where(['id' => $userId])->find();
        $shop_id = $members['shop_id'];

        if (empty($shop_id) || $shop_id == 0) {
            //查看是否是客服登录进来， 如果是客服，商家id 等于客服的父级的商家id
            if ($members['pid'] > 0) {
                $shop_id = db('member')->where(['id' => $members['pid']])->value('shop_id');
            }
        }
        if (empty($shop_id) || $shop_id == 0) {
            datamsg(400, '缺少商家id参数');
        }

        $ordouts = Db::name('order_timeout')->where('id', 1)->find();
        $webconfig = $this->webconfig;
        $perpage = 20;
        $offset = (input('post.page') - 1) * $perpage;

        $filter = input('post.filter');
        if (!$filter || !in_array($filter, array(1, 2, 3, 4, 5, 6, 7, 8, 9))) {
            $filter = 6;
        }

        switch ($filter) {
                //失败订单
            case 1:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 0, 'a.fh_status' => 0, 'a.order_status' => 2, 'a.is_show' => 1);
                $sort = array('a.addtime' => 'desc', 'a.id' => 'desc');
                break;
                //待发货
            case 2:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 1, 'a.fh_status' => 0, 'a.order_status' => 0, 'a.is_show' => 1);
                $sort = array('a.pay_time' => 'desc', 'a.id' => 'desc');
                break;
                //待收货
            case 3:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 1, 'a.fh_status' => 1, 'a.order_status' => 0, 'a.is_show' => 1);
                $sort = array('a.fh_time' => 'desc', 'a.id' => 'desc');
                break;
                //已完成订单(待评价)
            case 4:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 1, 'a.fh_status' => 1, 'a.order_status' => 1, 'a.ping' => 0, 'a.is_show' => 1);
                $sort = array('a.coll_time' => 'desc', 'a.id' => 'desc');
                break;
                //待付款
            case 5:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 0, 'a.fh_status' => 0, 'a.order_status' => 0, 'a.is_show' => 1);
                $sort = array('a.addtime' => 'desc', 'a.id' => 'desc');
                break;

                //失败订单
            case 7:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 0, 'a.order_status' => 2);
                $sort = array('a.addtime' => 'desc', 'a.id' => 'desc');
                break;
                //已评价
            case 8:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 1, 'a.fh_status' => 1, 'a.order_status' => 1, 'a.ping' => 1, 'a.is_show' => 1);
                $sort = array('a.addtime' => 'desc', 'a.id' => 'desc');
                break;
                //已完成（已收货）
            case 9:
                $where = array('a.shop_id' => $shop_id, 'a.state' => 1, 'a.fh_status' => 1, 'a.order_status' => 1, 'a.is_show' => 1);
                $sort = array('a.addtime' => 'desc', 'a.id' => 'desc');
                break;
                //全部
            case 6:
                $where = array('a.shop_id' => $shop_id, 'a.is_show' => 1);
                $sort = array('a.addtime' => 'desc', 'a.id' => 'desc');
                break;
        }

        if (in_array($filter, array(1, 2, 3, 4, 5, 6, 7, 8, 9))) {
            $orderes = Db::name('order')
                ->alias('a')
                ->field('a.id,a.ordernumber,a.coupon_id,a.total_price,a.state,a.fh_status,a.order_status,a.shouhou,a.ping,a.is_show,a.ping,a.order_type,a.pin_type,a.pin_id,a.shop_id,a.zdsh_time,a.time_out,b.shop_name')
                ->join('sp_shops b', 'a.shop_id = b.id', 'INNER')
                ->where($where)
                ->order($sort)
                ->count();
        } else {
            $orderes = Db::name('th_apply')
                ->alias('a')
                ->field('a.id,a.th_number,a.thfw_id,a.apply_status,a.tui_price,a.tui_num,a.orgoods_id,a.order_id,a.dcfh_status,a.sh_status,a.fh_status,a.shou_status,a.check_timeout,a.shoptui_timeout,a.yhfh_timeout,a.yhshou_timeout,a.shop_id,b.shop_name')
                ->join('sp_shops b', 'a.shop_id = b.id', 'INNER')
                ->where('a.user_id', $userId)
                ->order('a.apply_time desc')
                ->count();
        }

        datamsg(200, '获取订单信息成功', array('count' => $orderes));
    }

    //商家订单详情
    public function orderInfo()
    {
        $tokenRes = $this->checkToken();
        if ($tokenRes['status'] == 400) {
            datamsg(400, $tokenRes['mess'], $tokenRes['data']);
        } else {
            $userId = $tokenRes['user_id'];
        }

        $members = db('member')->where(['id' => $userId])->find();
        $shop_id = $members['shop_id'];

        if (empty($shop_id) || $shop_id == 0) {
            //查看是否是客服登录进来， 如果是客服，商家id 等于客服的父级的商家id
            if ($members['pid'] > 0) {
                $shop_id = db('member')->where(['id' => $members['pid']])->value('shop_id');
            }
        }
        if (empty($shop_id)) {
            datamsg(400, '缺少商家id参数');
        }
        if (input('post.order_num')) {
            $order_num = input('post.order_num');
            $orders = Db::name('order')
                ->alias('a')
                ->field('a.id,a.ordernumber,a.contacts,a.telephone,a.province,a.city,a.zf_type,a.area,a.address,a.goods_price,a.freight,a.youhui_price,a.coupon_id,a.coupon_price,a.coupon_str,a.total_price,a.beizhu,a.state,a.pay_time,a.fh_status,a.fh_time,a.order_status,a.shouhou,a.is_show,a.coll_time,a.can_time,a.ping,a.order_type,a.pin_type,a.pin_id,a.zong_id,a.shop_id,a.addtime,a.zdsh_time,a.time_out,b.order_number,c.shop_name,w.id as wid,w.psnum,w.ps_id')
                ->join('sp_order_zong b', 'a.zong_id = b.id', 'LEFT')
                ->join('order_wuliu w', 'a.id = w.order_id', 'LEFT')
                ->join('sp_shops c', 'a.shop_id = c.id', 'LEFT')
                ->where('a.ordernumber', $order_num)
                ->where('a.shop_id', $shop_id)
                ->where('a.is_show', 1)
                ->find();
            if (!$orders) {
                datamsg(400, '找不到相关订单');
            }

            $orders['paytype'] = get_pay_type($orders['zf_type']);
            //物流
            $orders['logistics'] = "";
            $wltype = Db::name('logistics')->field("log_name")->where('id', $orders['ps_id'])->find();
            if ($wltype) {
                $orders['logistics'] = $wltype['log_name'];
            }

            if ($orders['pay_time']) {
                $orders['pay_time'] = date('Y-m-d H:i:s', $orders['pay_time']);
            }

            if ($orders['fh_time']) {
                $orders['fh_time'] = date('Y-m-d H:i:s', $orders['fh_time']);
            }

            if ($orders['coll_time']) {
                $orders['coll_time'] = date('Y-m-d H:i:s', $orders['coll_time']);
            }

            if ($orders['can_time']) {
                $orders['can_time'] = date('Y-m-d H:i:s', $orders['can_time']);
            }

            if ($orders['addtime']) {
                $orders['addtime'] = date('Y-m-d H:i:s', $orders['addtime']);
            }

            if ($orders['state'] == 0 && $orders['fh_status'] == 0 && $orders['order_status'] == 0 && $orders['is_show'] == 1) {
                $orders['order_zt'] = "待付款";
                $orders['filter'] = 1;
                if ($orders['time_out'] > time()) {
                    $orders['sytime'] = time2string($orders['time_out'] - time());
                } else {
                    $orders['sytime'] = '';
                }
            } elseif ($orders['state'] == 1 && $orders['fh_status'] == 0 && $orders['order_status'] == 0 && $orders['is_show'] == 1) {
                $orders['order_zt'] = "待发货";
                $orders['filter'] = 2;
            } elseif ($orders['state'] == 1 && $orders['fh_status'] == 1 && $orders['order_status'] == 0 && $orders['is_show'] == 1) {
                $orders['order_zt'] = "待收货";
                $orders['filter'] = 3;
                if ($orders['sysh_time'] > time()) {
                    $orders['sysh_time'] = time2string($orders['zdsh_time'] - time());
                } else {
                    $orders['sysh_time'] = '';
                }
            } elseif ($orders['state'] == 1 && $orders['fh_status'] == 1 && $orders['order_status'] == 1 && $orders['is_show'] == 1) {
                $orders['order_zt'] = "已完成";
                $orders['filter'] = 4;
            } elseif ($orders['order_status'] == 2 && $orders['is_show'] == 1) {
                $orders['order_zt'] = "已关闭";
                $orders['filter'] = 5;
            }

            $orders['pinzhuangtai'] = 0;

            if ($orders['state'] == 1 && $orders['fh_status'] == 0 && $orders['order_status'] == 0 && $orders['order_type'] == 2 && $orders['is_show'] == 1) {
                $pinzts = Db::name('pintuan')
                    ->where('id', $orders['pin_id'])
                    ->where('state', 1)
                    ->field('id,pin_num,tuan_num,pin_status,timeout')
                    ->find();
                if ($pinzts) {
                    if ($pinzts['pin_status'] == 0) {
                        $order_assembleres = Db::name('order_assemble')
                            ->where('pin_id', $pinzts['id'])
                            ->where('order_id', $orders['id'])
                            ->where('user_id', $userId)
                            ->where('state', 1)
                            ->where('tui_status', 0)
                            ->find();
                        if ($order_assembleres) {
                            $orders['pinzhuangtai'] = 1;
                        } else {
                            $orders['pinzhuangtai'] = 2;
                        }
                    } elseif ($pinzts['pin_status'] == 2) {
                        $orders['pinzhuangtai'] = 2;
                    }
                } else {
                    $orders['pinzhuangtai'] = 2;
                }
            }

            $orders['goodsinfo'] = Db::name('order_goods')
                ->where('order_id', $orders['id'])
                ->field('id,goods_id,goods_name,thumb_url,goods_attr_str,real_price,goods_num,th_status,order_id')
                ->select();

            $webconfig = $this->webconfig;

            foreach ($orders['goodsinfo'] as $key => $val) {
                $orders['goodsinfo'][$key]['thumb_url'] = url_format($val['thumb_url'], $webconfig['weburl'], '?imageMogr2/thumbnail/200x200');
            }
            $orders['wulius'] = "";
            if ($orders['fh_status'] == 1) {
                $order_wulius = Db::name('order_wuliu')
                    ->alias('a')
                    ->field('a.id,a.psnum,b.log_name,b.telephone')
                    ->join('sp_logistics b', 'a.ps_id = b.id', 'LEFT')
                    ->where('a.order_id', $orders['id'])
                    ->find();
                if ($order_wulius) {
                    $orders['wulius'] = $order_wulius;
                }
            }
            datamsg(200, '获取订单详情成功', set_lang($orders));
        } else {
            datamsg(400, '缺少订单号');
        }
    }

    // 商家端订单申请发货
    public function orderApplyGood()
    {
        if (request()->isPost()) {
            if (input('post.order_id')) {
                $tokenRes = $this->checkToken();
                if ($tokenRes['status'] == 400) {
                    datamsg(400, $tokenRes['mess'], $tokenRes['data']);
                } else {
                    $userId = $tokenRes['user_id'];
                }

                $members = db('member')->where(['id' => $userId])->find();
                $shopId = $members['shop_id'];

                $orderId = input('post.order_id');
                $orderGoodss = db('order_goods')->where('order_id', $orderId)->select();
                $totalPrice = 0;

                $order_info = db('order')->where('id', $orderId)->find();
                if (!$order_info) {
                    datamsg(400, '订单不存在');
                }
                if ($order_info['state'] != 1 && $order_info['fh_status'] != 0 && $order_info['order_status'] != 0 && $order_info['source_type'] != 1) {
                    datamsg(400, '订单状态错误，商品无法申请发货');
                }

                foreach ($orderGoodss as $k => $v) {
                    if ($v['shop_id'] != $shopId) {
                        datamsg(400, '订单不属于您的店铺，无法申请发货');
                    }
                    $goodsInfo = db('goods')->where('id', $v['goods_id'])->find();
                    if (empty($v['goods_attr_str'])) {
                        $totalPrice += db('goods')->where('id', $goodsInfo['related_goods_id'])->value('shop_price') * $v['goods_num'];
                        if (!$totalPrice) {
                            datamsg(400, '产品库商品不存在，无法申请发货');
                        }
                    } else {
                        $title = [];
                        $goodsAttrStr = explode(";", $v['goods_attr_str']);
                        foreach ($goodsAttrStr as $k1 => $v1) {
                            $num = intval(strripos($v1, ':'));
                            $title[] = mb_substr($v1, $num + 1);
                        }
                        $title = implode('+', $title);
                        $goodsOption =  db('goods_option')->where(['goods_id' => $goodsInfo['related_goods_id'], 'title' => $title])->find();
                        if (!$goodsOption) {
                            datamsg(400, '产品库商品不存在，无法申请发货');
                        }
                        if ($goodsOption['is_seckill'] == 1) {
                            $totalPrice += $goodsOption['seckill_price']  * $v['goods_num'];
                        } elseif ($goodsOption['assemble_price'] == 1) {
                            $totalPrice += $goodsOption['assemble_price'] * $v['goods_num'];
                        } elseif ($goodsOption['is_integral'] == 1) {
                            $totalPrice += $goodsOption['integral_price']  * $v['goods_num'];
                        } else {
                            $totalPrice += $goodsOption['shop_price']  * $v['goods_num'];
                        }
                    }
                }

                $shopLevel = db('plugin_shoplevel_user')->where('shop_id', $shopId)->find();
                if (!$shopLevel) {
                    $rate = 20;
                } else {
                    $rate = db('plugin_shoplevel')->where('id', $shopLevel['shoplevel_id'])->value('rate');
                }

                $shopWallet = db('shop_wallet')->where('shop_id', $shopId)->find();
                $totalPrice = $totalPrice * (100 - $rate) / 100;
                if ($totalPrice > $shopWallet['price']) {
                    datamsg(400, '商家余额不足，请充值后申请发货！');
                }

                // 启动事务
                Db::startTrans();
                try {

                    //处理商家钱包明细
                    db('shop_wallet')->where('shop_id', $shopId)->setDec('price', $totalPrice);
                    $data = [];
                    $data['de_type'] = 2;
                    $data['zc_type'] = 2;
                    $data['price'] = $totalPrice;
                    $data['order_type'] = 1;
                    $data['order_id'] = $orderId;
                    $data['shop_id'] = $shopId;
                    $data['wat_id'] = $shopWallet['id'];
                    $data['time'] = time();

                    db('shop_detail')->insertGetId($data);


                    //处理平台钱包明细
                    db('pt_wallet')->where('id', 1)->setInc('price', $totalPrice);
                    $ptData = [];
                    $ptData['de_type'] = 1;
                    $ptData['sr_type'] = 2;
                    $ptData['price'] = $totalPrice;
                    $ptData['order_type'] = 6;
                    $ptData['order_id'] = $orderId;
                    $ptData['shop_id'] = $shopId;
                    $ptData['wat_id'] = 1;
                    $ptData['time'] = time();

                    db('pt_detail')->insertGetId($ptData);

                    db('order')->where('id', $orderId)->update(['source_type' => 2, 'shop_price' => $totalPrice, 'fh_status' => 1, 'fh_time' => time()]);
                    Db::commit();
                    datamsg(200, '申请发货成功');
                } catch (\Exception $e) {
                    // 回滚事务
                    Db::rollback();
                }
            } else {
                datamsg(400, '申请发货失败');
            }
        }
    }

    // 查询当前可用分类
    public function queryCate()
    {
        $cateData = db('category')->where(['is_show' => 1])->column('id,cate_name');
        datamsg(200, '获取分类成功', set_lang($cateData));
    }

    // 一键铺货 
    public function queryPuhuoGoods()
    {
        $tokenRes = $this->checkToken();
        if ($tokenRes['status'] == 400) {
            datamsg(400, $tokenRes['mess'], $tokenRes['data']);
        } else {
            $userId = $tokenRes['user_id'];
        }

        $members = db('member')->where(['id' => $userId])->find();
        $shopId = $members['shop_id'];

        if ($shopId == 1) {
            $value = array('status' => 0, 'mess' => '自营商铺无法铺货');
            return json($value);
        }

        // $currentGoodsIds = db('goods')->where('shop_id', $shopId)->column('related_goods_id');

        $where = [];
        if (input('cate_id')) {
            $where['cate_id'] = input('cate_id');
        }
        // 最低最高价格
        if (input('min_price') && input('max_price')) {
            $where['shop_price'] = ['between', [input('min_price'), input('max_price')]];
        } elseif (input('min_price')) {
            $where['shop_price'] = ['egt', input('min_price')];
        } elseif (input('max_price')) {
            $where['shop_price'] = ['elt', input('max_price')];
        }
        // 关键词
        if (input('keyword')) {
            $where['goods_name'] = ['like', '%' . input('keyword') . '%'];
        }

        $page = input('page');
        $perpage = 40;
        $offset = ($page - 1) * $perpage;

        $goodsData = db('goods')
            ->where('shop_id', 1)
            ->where('is_recycle', 0)
            ->where($where)
            ->where('id NOT IN (SELECT related_goods_id FROM sp_goods WHERE shop_id = ' . $shopId . ')')
            ->limit($offset, $perpage)
            ->column('id,goods_name,shop_price,market_price,thumb_url,cate_id');

        // 转为数组
        $goodsData = array_values($goodsData);

        datamsg(200, '获取产品库商品成功', set_lang($goodsData));
    }

    /**
     * 一键铺货功能
     * 将产品库中的商品复制一份到商家商品中
     ****/
    public function copyGoods()
    {
        $tokenRes = $this->checkToken();
        if ($tokenRes['status'] == 400) {
            datamsg(400, $tokenRes['mess'], $tokenRes['data']);
        } else {
            $userId = $tokenRes['user_id'];
        }

        $members = db('member')->where(['id' => $userId])->find();
        $shopId = $members['shop_id'];

        $onsale = input('onsale') ? input('onsale') : 0;
        $plugins = db('plugin')->where(['status' => 1, 'name' => 'through', 'isclose' => 1])->find();
        if ($plugins) {
            $type = input('type');
            $num = input('num');
        }
        $where = [];
        if (!empty(input('post.goods_id'))) {
            $goodsIdss = explode(',', input('post.goods_id'));
            $where['id'] = array('in', $goodsIdss);
        }
        if ($shopId == 1) {
            $value = array('status' => 0, 'mess' => '自营商铺无法铺货');
            return json($value);
        }

        // $check = empty(input('param.check'))?1:0;
        // //检查是否已有商品
        // $chenkgoods = Db::name("goods")->where(['shop_id'=>$shopId])->select();
        // if($chenkgoods && $check == 1){
        //     $value = array('status'=>0,'mess'=>'已有商品,确认要铺货吗？');
        //     return json($value);
        // }

        Db::startTrans();
        try {
            $repeatGoodsName = [];
            $goodsDb = db('goods');
            //取出一键铺货前商家商品ID集合
            $goodsIds = $goodsDb->where('shop_id', $shopId)->column('id');
            //取出产品库中商品
            $goodsData = $goodsDb->where($where)->where(['shop_id' => 1, 'is_recycle' => 0])->select();
            if ($goodsData) {
                $goodsIdata = [];
                foreach ($goodsData as $k => $v) {
                    //检查商品是否已经铺货
                    $goodsInfo = $goodsDb->where(['shop_id' => $shopId, 'related_goods_id' => $v['id']])->find();
                    if ($goodsInfo) {
                        $repeatGoodsName[] = $v['goods_name'];
                        continue;
                    }
                    $goodsIdata[$k] = $v;
                    $goodsIdata[$k]['shop_id'] = $shopId;
                    $goodsIdata[$k]['related_goods_id'] = $v['id'];
                    $goodsIdata[$k]['addtime'] = time();
                    if ($type == 0) {
                        $goodsIdata[$k]['shop_price'] = $v['shop_price'] + $num;
                        $goodsIdata[$k]['market_price'] = $v['market_price'] + $num + 1;
                    } elseif ($type == 1) {
                        $goodsIdata[$k]['shop_price'] = round($v['shop_price'] * ($num / 100 + 1), 2);
                        $goodsIdata[$k]['market_price'] = round($v['market_price'] * ($num / 100 + 1), 2) + 1;
                    }
                    $goodsIdata[$k]['onsale'] = $onsale;
                    unset($goodsIdata[$k]['id']);
                }

                $goodsModel = new GoodsModel();
                $goodsArrChunk = array_chunk($goodsIdata, 1000); //将一个数组分割多个数组
                foreach ($goodsArrChunk as $v) {
                    //此处批量插入数据操作
                    $goodsModel->saveAll($v);
                }

                //取出一键铺货后商家商品ID集合
                $newGoodsIds = $goodsDb->where('shop_id', $shopId)->column('id');
                //取出差集
                $newGoodsIds = array_diff($newGoodsIds, $goodsIds);
                $relatedGoodsIds = $goodsDb->where('id', 'in', $newGoodsIds)->column('related_goods_id');
                $newGoodsRes = $goodsDb->where('id', 'in', $newGoodsIds)->select();

                //改些商品列表数组将产品库ID作为数组的key
                $goodsRes = [];
                foreach ($newGoodsRes as $k => $v) {
                    $goodsRes[$v['related_goods_id']] = $v;
                }

                //处理商品属性
                $goodsAttrDb = Db::name('goods_attr');
                $goodsAttrData = $goodsAttrDb->where('goods_id', 'in', $relatedGoodsIds)->select();
                if ($goodsAttrData) {
                    $newGoodsAttrRes = [];
                    foreach ($goodsAttrData as $k => $v) {
                        $newGoodsAttrRes[$k] = $v;
                        $newGoodsAttrRes[$k]['goods_id'] = $goodsRes[$v['goods_id']]['id'];
                        unset($newGoodsAttrRes[$k]['id']);
                    }
                    $goodsAttrModel = new GoodsAttrModel();
                    $goodsAttrArrChunk = array_chunk($newGoodsAttrRes, 1000); //将一个数组分割多个数组
                    foreach ($goodsAttrArrChunk as $v) {
                        //此处批量插入数据操作
                        $goodsAttrModel->saveAll($v);
                    }
                }
                unset($goodsAttrData);
                unset($newGoodsAttrRes);
                unset($goodsAttrArrChunk);

                //处理商品可选参数信息表
                $goodsParamDb = Db::name('goods_param');
                $goodsParam = $goodsParamDb->where('goods_id', 'in', $relatedGoodsIds)->select();
                if ($goodsParam) {
                    $newGoodsParamRes = [];
                    foreach ($goodsParam as $k => $v) {
                        $newGoodsParamRes[$k] = $v;
                        $newGoodsParamRes[$k]['goods_id'] = $goodsRes[$v['goods_id']]['id'];
                        unset($newGoodsParamRes[$k]['id']);
                    }
                    $goodsParamModel = new GoodsParamModel();
                    $goodsParamArrChunk = array_chunk($newGoodsParamRes, 1000); //将一个数组分割多个数组
                    foreach ($goodsParamArrChunk as $v) {
                        //此处批量插入数据操作
                        $goodsParamModel->saveAll($v);
                    }
                }
                unset($goodsParam);
                unset($newGoodsParamRes);
                unset($goodsParamArrChunk);

                //处理商品关联语言
                $goodsLangDb = Db::name('goods_lang');
                $goodsLang = $goodsLangDb->where('goods_id', 'in', $relatedGoodsIds)->select();
                if ($goodsLang) {
                    $newGoodsLangRes = [];
                    foreach ($goodsLang as $k => $v) {
                        $newGoodsLangRes[$k] = $v;
                        $newGoodsLangRes[$k]['goods_id'] = $goodsRes[$v['goods_id']]['id'];
                        unset($newGoodsLangRes[$k]['id']);
                    }
                    $goodsLangModel = new GoodsLangModel();
                    $goodsLangArrChunk = array_chunk($newGoodsLangRes, 1000); //将一个数组分割多个数组
                    foreach ($goodsLangArrChunk as $v) {
                        //此处批量插入数据操作
                        $goodsLangModel->saveAll($v);
                    }
                }
                unset($goodsLang);
                unset($newGoodsLangRes);
                unset($goodsLangArrChunk);

                //处理商品图片
                $goodsPicDb = Db::name('goods_pic');
                $goodsPic = $goodsPicDb->where('goods_id', 'in', $relatedGoodsIds)->select();
                $newGoodsPicRes = [];
                foreach ($goodsPic as $k => $v) {
                    $newGoodsPicRes[$k] = $v;
                    $newGoodsPicRes[$k]['goods_id'] = $goodsRes[$v['goods_id']]['id'];
                    unset($newGoodsPicRes[$k]['id']);
                }
                $goodsPicModel = new GoodsPicModel();
                $goodsPicArrChunk = array_chunk($newGoodsPicRes, 1000); //将一个数组分割多个数组
                foreach ($goodsPicArrChunk as $v) {
                    //此处批量插入数据操作
                    $goodsPicModel->saveAll($v);
                }
                unset($goodsPic);
                unset($newGoodsPicRes);
                unset($goodsPicArrChunk);

                //处理商品规格详情信息表
                $goodsOptionDb = Db::name('goods_option');
                $goodsOption = $goodsOptionDb->where('goods_id', 'in', $relatedGoodsIds)->select();
                $newGoodsOptionRes = [];
                if ($goodsOption) {
                    foreach ($goodsOption as $k => $v) {
                        $newGoodsOptionRes[$k] = $v;
                        $newGoodsOptionRes[$k]['goods_id'] = $goodsRes[$v['goods_id']]['id'];
                        if ($type == 0) {
                            $newGoodsOptionRes[$k]['shop_price'] = $newGoodsOptionRes[$k]['shop_price'] + $num;
                            $newGoodsOptionRes[$k]['market_price'] = $newGoodsOptionRes[$k]['market_price'] + $num + 1;
                        } elseif ($type == 1) {
                            $newGoodsOptionRes[$k]['shop_price'] = round($newGoodsOptionRes[$k]['shop_price'] * ($num / 100 + 1), 2);
                            $newGoodsOptionRes[$k]['market_price'] = round($newGoodsOptionRes[$k]['market_price'] * ($num / 100 + 1), 2);
                        }

                        unset($newGoodsOptionRes[$k]['id']);
                    }
                }
                -$goodsOptionModel = new GoodsOptionModel();
                $goodsOptionArrChunk = array_chunk($newGoodsOptionRes, 1000); //将一个数组分割多个数组
                foreach ($goodsOptionArrChunk as $v) {
                    //此处批量插入数据操作
                    $goodsOptionModel->saveAll($v);
                }

                //处理商品规格
                $goodsSpecDb = db('goods_spec');
                $goodsSpec = $goodsSpecDb->where('goods_id', 'in', $relatedGoodsIds)->select();
                $newGoodsSpecRes = [];
                if ($goodsSpec) {
                    foreach ($goodsSpec as $k => $v) {
                        $newGoodsSpecRes[$k] = $v;
                        $newGoodsSpecRes[$k]['goods_id'] = $goodsRes[$v['goods_id']]['id'];
                        unset($newGoodsSpecRes[$k]['id']);
                    }
                }
                $goodsSpecModel = new GoodsSpecModel();
                $goodsSpecArrChunk = array_chunk($newGoodsSpecRes, 1000); //将一个数组分割多个数组
                foreach ($goodsSpecArrChunk as $v) {
                    //此处批量插入数据操作
                    $goodsSpecModel->saveAll($v);
                }
                unset($goodsSpec);
                unset($newGoodsSpecRes);
                unset($goodsSpecArrChunk);

                //处理商品规格项
                $goodsSpecItemDb = db('goods_spec_item');
                $goodsSpecsIdsGoodsIds = $goodsSpecDb->where('goods_id', 'in', $relatedGoodsIds)->field('id,goods_id')->select();
                $newGoodsSpecsIdsGoodsIds = $goodsSpecDb->where('goods_id', 'in', $newGoodsIds)->field('id,goods_id')->select();
                $goodsSpecsIds = $goodsSpecDb->where('goods_id', 'in', $relatedGoodsIds)->column('id');

                $ids = [];
                foreach ($goodsSpecsIdsGoodsIds as $k => $v) {
                    $ids[$v['id']] = $newGoodsSpecsIdsGoodsIds[$k]['id'];
                }
                $newGoodsSpecItems = [];
                $goodsSpecItems = $goodsSpecItemDb->where('spec_id', 'in', $goodsSpecsIds)->select();
                foreach ($goodsSpecItems as $k => $v) {
                    $newGoodsSpecItems[$k] = $v;
                    $newGoodsSpecItems[$k]['spec_id'] = $ids[$v['spec_id']];
                    unset($newGoodsSpecItems[$k]['id']);
                }
                $goodsSpecItemModel = new GoodsSpecItemModel();
                $goodsSpecItemArrChunk = array_chunk($newGoodsSpecItems, 1000); //将一个数组分割多个数组
                foreach ($goodsSpecItemArrChunk as $v) {
                    //此处批量插入数据操作
                    $goodsSpecItemModel->saveAll($v);
                    unset($v);
                }
                unset($goodsSpecItems);
                unset($newGoodsSpecItems);
                unset($goodsSpecItemArrChunk);


                //取出商品规格信息
                $spec = [];
                $option = [];
                foreach ($newGoodsRes as $k => $v) {
                    $goodsSpecIds = $goodsSpecDb->where('goods_id', $v['related_goods_id'])->column('id');
                    $newGoodsSpecIds = $goodsSpecDb->where('goods_id', $v['id'])->column('id');

                    $newGoodsSpecRes = $goodsSpecDb->where('goods_id', $v['id'])->select();

                    $itemIds = $goodsSpecItemDb->where('spec_id', 'in', $goodsSpecIds)->column('id');
                    $newItemIds = $goodsSpecItemDb->where('spec_id', 'in', $newGoodsSpecIds)->column('id');

                    //取出规格详情信息
                    foreach ($newGoodsSpecRes as $k2 => $v2) {
                        $content = str_replace($itemIds, $newItemIds, $v2['content']);
                        $spec[] = ['id' => $v2['id'], 'content' => $content];
                        //                        $goodsSpecDb->where('id',$v2['id'])->update(['content'=>$content]);
                        $goodsSpec = Db::name("goods_option")->where(['goods_id' => $v2['goods_id']])->select();
                        foreach ($goodsSpec as $k1 => $v1) {
                            $specs = str_replace($itemIds, $newItemIds, $v1['specs']);
                            //                            $goodsOptionDb->where('id',$v1['id'])->update(['specs'=>$specs]);
                            $option[] = ['id' => $v1['id'], 'specs' => $specs];
                        }
                    }
                }

                $specArrChunk = array_chunk($spec, 1000); //将一个数组分割多个数组
                foreach ($specArrChunk as $v) {
                    //此处批量插入数据操作
                    $goodsSpecModel->isUpdate()->saveAll($v);
                    unset($v);
                }

                $optionArrChunk = array_chunk($option, 1000); //将一个数组分割多个数组
                foreach ($optionArrChunk as $v) {
                    //此处批量插入数据操作
                    $goodsOptionModel->isUpdate()->saveAll($v);
                    unset($v);
                }
            }
            Db::commit();
            if (!empty($repeatGoodsName) && !empty(input('post.goods_id'))) {
                datamsg(200, '共' . (count($goodsIdss) - count($repeatGoodsName)) . '件商品铺货成功' . count($repeatGoodsName) . '件商品已存在、无法再次铺货！' . implode(' | ', $repeatGoodsName));
            } else {
                datamsg(200, '铺货成功');
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            Db::rollback();
            dump($e->getMessage());

            datamsg(400, '铺货失败');
            die;
        }
    }
}
