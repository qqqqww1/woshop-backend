<?php
namespace app\api\controller;
use app\api\model\Config as ConfigModel;
use think\Db;


class Config extends Common
{
    /*
     * 获取过审配置信息
     */
    public function getShowConfig(){
        $tokenRes = $this->checkToken(0);
        if($tokenRes['status'] == 400){
            datamsg(400,$tokenRes['mess']);
        }
        $configModel = new ConfigModel();
        $examineInfo = $configModel->getExamine();
        datamsg(200,'获取配置信息',$examineInfo);
    }

    /**
     * 获取配置信息
     */
    public function getConfig(){
        $tokenRes = $this->checkToken(0);
        if($tokenRes['status'] == 400){
            datamsg(400,$tokenRes['mess']);
        }
        $ename = [
            'web_url_type',
            'web_url'
        ];
        $getList = Db::name("config")->whereIn("ename",$ename)->select();
        foreach ($getList as $key=>$value){
            $getList[$value['ename']] = $value;
        }
        $responseResult = [];
        foreach ($ename as $k=>$v){
            if(!isset($getList[$v]['value'])){
                $vlue = false;
            }else{
                if($getList[$v]['value'] == '0'){
                    $value = false;
                }else{
                    $value = $getList[$v]['value'];
                }
            }
            $responseResult[$v] =  $value;
        }
        datamsg(200,'获取成功',$responseResult);//$responseResult
    }


    /**
     * 根据ip获取国家位置
     */
    public function getIpLocation(){
        $ip = getIP();
        $responseResult = file_get_contents("http://ip.taobao.com/outGetIpInfo?ip={$ip}&accessKey=alibaba-inc");
        $res = json_decode($responseResult,true);
        if($res){
            $data = $res['data'];
            $country = Db::name("country")->where("country_cname",$data['country'])->find();
            if(!$country){
                $country = Db::name("country")->where("country_cname","中国")->find();
            }
        }else{
            $country = Db::name("country")->where("country_cname","中国")->find();
        }
        datamsg(200,'获取成功',$country);
    }

}