<?php
namespace app\api\controller;
use think\Controller;
use think\Db;

class UsdtConfig extends Common{


    //获取USDT支付参数
    public function getUsdtConfig(){

        $tokenRes = $this->checkToken(0);
        if($tokenRes['status'] == 400){
            datamsg(400,$tokenRes['mess']);
        }

        $type = input('post.type');
        if(empty($type)){
            datamsg(400,'缺少USDT支付类型');
        }
        $usdtConfigRes = db('usdt_config')->where(['id'=>1])->find();
        $webconfig = $this->webconfig;
        if($type == 1){
            $usdtConfig = [];
            $usdtConfig['wallet'] = $usdtConfigRes['TRC20_wallet'];
            $usdtConfig['pic'] = url_format($usdtConfigRes['TRC20_pic'],$webconfig['weburl']);

        }elseif($type == 2){
            $usdtConfig = [];
            $usdtConfig['wallet'] = $usdtConfigRes['ERC20_wallet'];
            $usdtConfig['pic'] = url_format($usdtConfigRes['ERC20_pic'],$webconfig['weburl']);
        }
        datamsg(200,'获取USDT支付参数成功',$usdtConfig);
    }


}