<?php
namespace app\api\controller;


use think\Db;

class Raffle extends Common{

    //判断平台是否安装了抽奖插件
    public function getPluginInfo(){
        $where['status'] = 1;
        $where['isclose'] = 1;
        $where['name'] = 'raffle';
        $plugins=db('plugin')->where($where)->find();
        $responseResult['isRaffle'] = $plugins ? true : false;
        datamsg(200,'获取成功',$responseResult);
    }




    //获取奖项列表
    public function getRaffleList(){
        $tokenRes = $this->checkToken(0);
        if($tokenRes['status'] == 400){
            datamsg(400,$tokenRes['mess'],$tokenRes['data']);
        }
        $plugins=db('plugin')->where('name','raffle')->find();
        if(!$plugins){
            datamsg(400,'未安装插件',array('status'=>400));
        }
        $webconfig = $this->webconfig;

        $raffleLst=db('plugin_raffle')->field("id,name,img,color,num,type")->where('is_show',1)->order('id esc')->select();
        foreach ($raffleLst as $k =>$v){
            $raffleLst[$k]['img'] = url_format($v['img'],$webconfig['weburl']);
            $raffleLst[$k]['Color'] = $v['color'];
        }
        datamsg(200,'获取奖项信息成功',set_lang($raffleLst));
    }

    //用户抽奖
    public function postUserRaffle(){
        $tokenRes = $this->checkToken();
        if($tokenRes['status'] == 400){
            datamsg(400,$tokenRes['mess'],$tokenRes['data']);
        }else{
            $userId = $tokenRes['user_id'];
        }
        $plugins=db('plugin')->where('name','raffle')->find();
        if(!$plugins){
            datamsg(400,'未安装插件',array('status'=>400));
        }

        $userIntegral = db('member')->where('id',$userId)->value('integral');
        $consumeIntegral = db('plugin_raffle_config')->where('ename','consume_integral')->value('value');
        if($userIntegral < $consumeIntegral){
            datamsg(400,'积分不足',array('status'=>400));
        }

        $userNum =  db('plugin_raffle_user')->where('user_id',$userId)->whereTime('created_at', 'today')->count();
        $userNumConfig = db('plugin_raffle_config')->where('ename','user_num')->value('value');

        if($userNum >= $userNumConfig){
            datamsg(400,'今日抽奖次数已用尽',array('status'=>400));
        }

        $raffleLst=db('plugin_raffle')->where('is_show',1)->order('id esc')->select();
        if(empty($raffleLst)){
            datamsg(400,'后台管理员没有配置奖励',array('status'=>400));
        }
        //抽奖
        foreach ($raffleLst as $key => $val) {
            $arr[$val['id']] = $val['rate'];//概率数组
        }
        $rid = $this->get_rand($arr); //根据概率获取奖项id

        $rafflInfo = db('plugin_raffle')->where('id',$rid)->find();
        if($rafflInfo){
            if($rafflInfo['type'] == 0){
                datamsg(200,'抽奖成功！',$rafflInfo);
            }
        }

        Db::startTrans();
        try{
            $data = [];
            $data['user_id'] = $userId;
            $data['num'] = $rafflInfo['num'];
            $data['name'] = $rafflInfo['name'];
            $data['rate'] = $rafflInfo['rate'];
            $data['raffle_id'] = $rafflInfo['id'];
            $data['created_at'] = time();
            db('plugin_raffle_user')->insert($data);
            Db::commit();
            //处理用户积分
            db('member')->where('id',$userId)->setDec('integral',$consumeIntegral);
            $memberIntegral = [];
            $memberIntegral['user_id'] = $userId;
            $memberIntegral['integral'] = $consumeIntegral;
            $memberIntegral['type'] = 15;
            $memberIntegral['addtime'] = time();
            $memberIntegral['class'] = 1;
            db('member_integral')->insert($memberIntegral);
            datamsg(200,'抽奖成功！',$rafflInfo);
        } catch (\Exception $e) {
            dump($e->getMessage());die;
            datamsg(400,'抽奖失败！');
        }
    }

    //计算中奖概率
    function get_rand($proArr) {
        $result = '';
        //概率数组的总概率精度
        $proSum = array_sum($proArr);
        // var_dump($proSum);
        //概率数组循环
        foreach ($proArr as $key => $proCur) {

            $randNum = mt_rand(1, $proSum); //返回随机整数

            if ($randNum <= $proCur) {
                $result = $key;
                break;
            } else {
                $proSum -= $proCur;
            }
        }
        unset ($proArr);
        return $result;
    }

}