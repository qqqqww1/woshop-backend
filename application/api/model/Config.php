<?php

namespace app\api\model;

use think\Model;

class Config extends Model
{
    public function getExamine(){
        $where = [
            'open_many_shop',
            'copyright_open',
            'open_or_not',
            'is_email',
            'is_phone',
            'is_zf_phone',
        ];
        $examineInfo = $this->where('ename','in',$where)->field('id,cname,ename,value')->select();
        $plugins=db('plugin')->where('name','Lang')->find();
        if($plugins){
            $lang= db('lang')->where('is_default',1)->value('lang_code');
            array_push($examineInfo,$lang);
        }
        return $examineInfo;
    }
}
