<?php
/**
 * @Description: 开发场景配置文件
 * @Author : 梧桐 <2487937004@qq.com>
 * @Copyright : 武汉一一零七科技有限公司 All rights reserved.
 */
namespace app\api\model;
use think\Model;
use think\Db;

class Shops extends Model
{
    public function getRecommendShops($num){
        $shops =  Db::name('shops')
            ->where(['open_status'=>1,'normal'=>1])
            ->order('update_time','desc')
            ->limit($num)
            ->select();
        return $shops;
    }

    public function getRecommendShops1($num){
        $myShop = Db::name("shops")
            ->where(['open_status'=>1,'normal'=>1])
            ->whereIn('id',[39,40])
            ->order('update_time','desc')
            ->select();
        if($myShop){
            $num = 10;
        }
        dump($myShop);die;
        $allShop =  Db::name('shops')
            ->where(['open_status'=>1,'normal'=>1])
            ->order('update_time','desc')
            ->limit($num)
            ->select();
        if($myShop && $allShop){
            $shops = array_merge($myShop,$allShop);
        }else{
            $shops = $allShop;
        }
        return $shops;
    }
}