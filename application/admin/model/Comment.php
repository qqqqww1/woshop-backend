<?php

namespace app\admin\model;
use think\Model;

class Comment extends Model{

    /**
     * 计算店铺商品、服务、物流评分
     * @param $shop_id      店铺id
     * @param $star_name    评价名称
     * @param int $type     0：商品、服务、物流 1：店铺好评率
     * @return string
     * @throws \think\Exception
     */
    public function getStar($shop_id,$star_name,$type=0){

        $where = [];
        if($type == 0){
            $where['shop_id']   =   $shop_id;
            $where['checked']   =   1;
        }elseif($type == 1){
            $where['shop_id']   =   $shop_id;
            $where['checked']   =   1;
            $where['goods_star']   =   array('>',3);
        }

        $count  =   $this->where($where)->count($star_name);
        $num    =   $this->where($where)->sum($star_name);

        if($type == 0){
            $star = round($num/$count,1);
        }elseif ($type == 1){
            unset($where['goods_star']);
            $star = [];
            $countZong = $this->where($where)->count($star_name);
            $star['count'] = $count;
            $star['zong'] = $countZong;
        }
        return $star;
    }
}
