<?php

namespace app\admin\controller;
use app\admin\controller\Common;
use app\admin\model\NewPublish as NewPublishModel;
use app\admin\model\Shops;
use app\admin\model\Goods;
use app\admin\model\Video;
use app\common\Lookup;
use think\Db;

class NewPublish extends Common{
    
    public $newModel;
    
    public function _initialize() {
        parent::_initialize();
        $this->newModel = new NewPublishModel();
    }
    
    public function lst() {
        $keyword = input('keyword');
        $list = $this->newModel->getNewPublishList($keyword, Lookup::pageSize);
        $goodsModel = new Goods();
        foreach ($list as $key => $item) {
            $goods_list = $goodsModel->getGoodsInfo($item['goods_id']);
            $goods_name_str = '';
            foreach ($goods_list as $v) {
                 $goods_name_str .= '【' . $v['goods_name'] . '】<br>';
            }
            $list[$key]['goods_name'] = $goods_name_str;
        }
        $page = $list->render();
        $data = array(
            'list' => $list,
            'page' => $page,
            'keyword' => $keyword,
        );
        $this->assign($data);
        return request()->isAjax() ? $this->fetch('ajaxpage') : $this->fetch();
    }

    public function add() {
        if (request()->isPost()) {
            $data = input('post.');
            $publishData = [];
            $lang = db('lang')->select();
            foreach ($lang as $k => $v){
                if(!empty($data['title_'.$v['lang_code']])){
                    $publishData['title'] = $data['title_'.$v['lang_code']];
                }
                if(!empty($data['content_'.$v['lang_code']])){
                    $publishData['content'] = $data['content_'.$v['lang_code']];
                }
                if(!empty($data['desc_'.$v['lang_code']])){
                    $publishData['desc'] = $data['desc_'.$v['lang_code']];
                }
                break;
            }
            $publishData['shop_id'] = $data['shop_id'];
            $publishData['praise_num'] = $data['praise_num'];
            $publishData['read_num'] = $data['read_num'];
            $publishData['status'] = $data['status'];
            $publishData['goods_id'] = implode(',', $data['goods_id']);
            $result = $this->validate($publishData, 'NewPublish');
            if(true !== $result){
                return json(array('status' => Lookup::isClose, 'mess' => $result));
            }
            $count = count($data['goods_id']) + count($data['video_id']);
            if ($count > 9) {
                return json(array('status' => Lookup::isClose, 'mess' => '商品和视频共计不超过9个'));
            }

            $addResult = $this->newModel->save($publishData);
            if (!$addResult) {
                return json(array('status' => Lookup::isClose, 'mess' => '添加失败'));
            }
            $newPublishLangDb = db('new_publish_lang');
            $langs = db('lang')->select();
            foreach ($langs as $k => $v) {
                $newPublishData = [];
                $newPublishData['new_publish_id'] = $this->newModel->id;
                $newPublishData['lang_id']  = $v['id'];
                $newPublishData['title'] = $data['title_'. $v['lang_code']];
                $newPublishData['content'] = $data['content_'. $v['lang_code']];
                $newPublishData['desc'] = $data['desc_'. $v['lang_code']];
                // 判断商品多语言设置是否存在，没有就新增，存在就更新
                $hasGoodsLang = $newPublishLangDb->where(['new_publish_id'=>$this->newModel->id,'lang_id'=>$v['id']])->find();
                if($hasGoodsLang){
                    $newPublishLangDb->where(['new_publish_id'=> $this->newModel->id,'lang_id'=>$v['id']])->update($newPublishData);
                }else{
                    $newPublishLangDb->insertGetId($newPublishData);
                }
            }

            return json(array('status' => Lookup::isOpen,'mess' => '添加成功'));
        }
        $shopsModel = new Shops();
        $shop_list = $shopsModel->getShopList();
        $langs      = Db::name('lang')->order('id asc')->select();
        $this->assign('langs', $langs);
        $data = array('shop_list' => $shop_list);
        $this->assign($data);
        return $this->fetch();
    }

    public function edit() {
        if (request()->isPost()) {
            $data = input('post.');
            $publishData = [];
            $lang = db('lang')->select();
            foreach ($lang as $k => $v){
                if(!empty($data['title_'.$v['lang_code']])){
                    $publishData['title'] = $data['title_'.$v['lang_code']];
                }
                if(!empty($data['content_'.$v['lang_code']])){
                    $publishData['content'] = $data['content_'.$v['lang_code']];
                }
                if(!empty($data['desc_'.$v['lang_code']])){
                    $publishData['desc'] = $data['desc_'.$v['lang_code']];
                }
                break;
            }
            $publishData['shop_id'] = $data['shop_id'];
            $publishData['praise_num'] = $data['praise_num'];
            $publishData['read_num'] = $data['read_num'];
            $publishData['status'] = $data['status'];
            $publishData['goods_id'] = implode(',', $data['goods_id']);
            $publishData['create_time'] = time();

            $result = $this->validate($publishData, 'NewPublish');
            if(true !== $result){
                return json(array('status' => Lookup::isClose, 'mess' => $result));
            }
            $count = count($data['goods_id']) + count($data['video_id']);
            if ($count > 9) {
                return json(array('status' => Lookup::isClose, 'mess' => '商品和视频共计不超过9个'));
            }

            // 启动事务
            Db::startTrans();
            try{
                db('new_publish')->where('id',$data['id'])->update($publishData);
                // 提交事务
                Db::commit();
                $newPublishLangDb = db('new_publish_lang');
                $langs = db('lang')->select();
                foreach ($langs as $k => $v) {
                    $newPublishData = [];
                    $newPublishData['new_publish_id'] = $data['id'];
                    $newPublishData['lang_id']  = $v['id'];
                    $newPublishData['title'] = $data['title_'. $v['lang_code']];
                    $newPublishData['content'] = $data['content_'. $v['lang_code']];
                    $newPublishData['desc'] = $data['desc_'. $v['lang_code']];
                    // 判断商品多语言设置是否存在，没有就新增，存在就更新
                    $hasGoodsLang = $newPublishLangDb->where(['new_publish_id'=>$data['id'],'lang_id'=>$v['id']])->find();
                    if($hasGoodsLang){
                        $newPublishLangDb->where(['new_publish_id'=> $data['id'],'lang_id'=>$v['id']])->update($newPublishData);
                    }else{
                        $newPublishLangDb->insertGetId($newPublishData);
                    }
                }
                return json(array('status' => Lookup::isOpen,'mess' => '编辑成功'));
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                return json(array('status' => Lookup::isClose, 'mess' => '编辑失败'));
            }
        }
        $id = input('id');
        $info = $this->newModel->getNewPublishInfo($id);
        $shopsModel = new Shops();
        $shop_list = $shopsModel->getShopList();
        $goodsModel = new Goods();
        $goods_list = $goodsModel->getGoodsListByIds($info['goods_id']);
        $langs      = Db::name('lang')->order('id asc')->select();
        $newPublishLangs = Db::name('new_publish_lang')->where('new_publish_id',$id)->select();

        $this->assign('langs', $langs);
        $this->assign('newPublishLangs', $newPublishLangs);

        $data = array(
            'info' => $info,
            'shop_list' => $shop_list,
            'goods_list' => $goods_list,
        );
        $this->assign($data);
        return $this->fetch();
    }
    
    public function delete() {
        if (!request()->isAjax()) {
            return json(array('status' => Lookup::isClose, 'mess' => '请求方式错误'));
        }
        $id = input('id');
        if (!$id) {
            return json(array('status' => Lookup::isClose, 'mess' => '参数错误'));
        }
        $delResult = $this->newModel->destroy($id);
        if (!$delResult) {
            return json(array('status' => Lookup::isClose, 'mess' => '删除失败'));
        }
        return json(array('status' => Lookup::isOpen, 'mess' => '删除成功'));
    }
    
    public function changeStatus() {
        if (!request()->isPost()) {
            return json(array('status' => 0, 'mess' => '请求方式错误'));
        }
        $id = input('post.id');
        if (!$id) {
            return json(array('status' => 0, 'mess' => '参数错误'));
        }
        $status = abs(input('post.status') - 1);
        $data = array('status' => $status);
        $updateResult = $this->newModel->update($data, array('id' => $id));
        if (!$updateResult) {
            return json(array('status' => 0, 'mess' => '修改失败'));
        }
        return json(array('status' => 1, 'mess' => '修改成功'));
    }
    
}
