<?php
namespace app\admin\validate;
use think\Validate;

class ShopAdmin extends Validate
{
    protected $rule = [
        'phone' => 'require|unique:shop_admin',
        'xieyi' => 'require|in:0,1',
        'password'=>'require'
    ];

    protected $message = [
        'phone.require' => '用户名不能为空',
        'phone.unique' => '用户名已存在',
        'xieyi.require' => '请同意注册协议',
        'xieyi.in' => '同意注册协议参数错误',
        'password' =>'密码不能为空',
    ];

}