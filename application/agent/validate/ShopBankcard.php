<?php
namespace app\agent\validate;
use think\Validate;

class ShopBankcard extends Validate

{
    protected $rule = [
        'user_id' => 'require|number',
    ];
    
    protected $message = [
        'user_id.require' => '缺少代理商ID参数',
        'user_id.number' => '代理商ID参数格式错误',
    ];
    
    

}