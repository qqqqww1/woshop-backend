<?php
namespace app\agent\controller;
use app\agent\controller\Common;
use think\Db;

class ShopsFinance extends Common{
    
    public function lst(){
        $agent_id = session('agent_id');

        $list = Db::name('shops')->alias('a')->field('a.id,a.shop_name,a.contacts,a.telephone,a.open_status,a.addtime,b.price,c.industry_name,d.pro_name,f.city_name,u.area_name')->join('sp_shop_wallet b','a.id = b.shop_id','LEFT')->join('sp_industry c','a.indus_id = c.id','LEFT')->join('sp_province d','a.pro_id = d.id','LEFT')->join('sp_city f','a.city_id = f.id','LEFT')->join('sp_area u','a.area_id = u.id','LEFT')->where('a.id','neq',1)->where('a.agent_id',$agent_id)->order('a.addtime desc')->paginate(25)->each(function($item,$k){
            $item['totalTurnover'] =  Db::name('order')->where('state','1')->where('shop_id',$item['id'])->sum('goods_price');
            return $item;
        });
        $count = Db::name('shops')->where('id','neq',1)->where('agent_id',$agent_id)->count();
        $page = $list->render();
    
        if(input('page')){
            $pnum = input('page');
        }else{
            $pnum = 1;
        }
    
        $prores = Db::name('province')->field('id,pro_name,zm')->order('sort asc')->select();
        $industryres = Db::name('industry')->where('is_show',1)->field('id,industry_name')->order('sort asc')->select();
        $this->assign(array(
            'list'=>$list,
            'count'=>$count,
            'page'=>$page,
            'pnum'=>$pnum,
            'prores'=>$prores,
            'industryres'=>$industryres
        ));
        
        if(request()->isAjax()){
            return $this->fetch('ajaxpage');
        }else{
            return $this->fetch('lst');
        }
    }
    
    //修改状态
    public function gaibian(){
        $id = input('post.id');
        $name = input('post.name');
        $value = input('post.value');
        $data[$name] = $value;
        $data['id'] = $id;
        $count = Db::name('shops')->update($data);
        if($count > 0){
            ys_admin_logs('改变商家开启或关闭状态','shops',$id);
            $result = 1;
        }else{
            $result = 0;
        }
        return $result;
    }
    
    public function getcitylist(){
        if(request()->isPost()){
            $pro_id = input('post.pro_id');
            if($pro_id){
                $cityres = Db::name('city')->where('pro_id',$pro_id)->field('id,city_name,zm')->order('sort asc')->select();
                if(empty($cityres)){
                    $cityres = 0;
                }
                return $cityres;
            }
        }
    }
    
    public function getarealist(){
        if(request()->isPost()){
            $city_id = input('post.city_id');
            if($city_id){
                $areares = Db::name('area')->where('city_id',$city_id)->field('id,area_name,zm')->order('sort asc')->select();
                if(empty($areares)){
                    $areares = 0;
                }
                return $areares;
            }
        }
    }
    
    
    public function info(){
        $shop_id = input('shop_id');

        $monthTotalTurnover = [];

        // 前12个月销售量
        for ($i=0; $i <= 12; $i++) {
            $totalTurnover = Db::name('order')->where('state','1')->where('shop_id',$shop_id)->where("FROM_UNIXTIME(addtime,'%m') = ".date('m',strtotime('-'.$i.' month')))->sum('goods_price');
            $monthTotalTurnover[date('y',strtotime('-'.$i.' month')).'年'.date('m',strtotime('-'.$i.' month')).'月']= $totalTurnover;
        }
        $this->assign(array(
            'monthTotalTurnover'=>$monthTotalTurnover,
            'shop_id' => $shop_id
        ));

        if(request()->isAjax()){
            return $this->fetch('info_ajaxpage');
        }else{
            return $this->fetch('info');
        }
    }

    public function edit(){
        if(request()->isPost()){
            $data = input('post.');
            $admin_id = session('admin_id');
            $result = $this->validate($data,'Shops');
            if(true !== $result){
                $value = array('status'=>0,'mess'=>$result);
            }else{
                $shops = Db::name('shops')->where('id',$data['id'])->field('id,logo')->find();
                if($shops){
                    $pro_id = $data['pro_id'];
                    $city_id = $data['city_id'];
                    $area_id = $data['area_id'];
                    $pros = Db::name('province')->where('id',$pro_id)->where('checked',1)->where('pro_zs',1)->field('id,pro_name')->find();
                    if($pros){
                        $citys = Db::name('city')->where('id',$city_id)->where('pro_id',$pros['id'])->where('checked',1)->where('city_zs',1)->field('id,city_name')->find();
                        if($citys){
                            $areas = Db::name('area')->where('id',$area_id)->where('city_id',$citys['id'])->where('checked',1)->field('id,area_name')->find();
                            if($areas){
                                $data['latlon'] = str_replace('，', ',', $data['latlon']);
                                if(strpos($data['latlon'],',') !== false){

                                    $latlon = explode(',', $data['latlon']);

                                    if(!empty($data['pic_id'])){
                                        $data['logo'] = $data['pic_id'];
                                    }else{
                                        if(!empty($shops['logo'])){
                                            $data['logo'] = $shops['logo'];
                                        }else{
                                            $data['logo'] = '';
                                        }
                                    }
                                    if(empty($data['wxnum'])){
                                        $data['wxnum'] = '';
                                    }

                                    if(empty($data['qqnum'])){
                                        $data['qqnum'] = '';
                                    }

                                    if(empty($data['sertime'])){
                                        $data['sertime'] = '';
                                    }

                                    $data['search_keywords'] = str_replace('，', ',', $data['search_keywords']);
                                    $count = Db::name('shops')->update(array(
                                        'shop_name'=>$data['shop_name'],
                                        'indus_id'=>$data['indus_id'],
                                        'shop_desc'=>$data['shop_desc'],
                                        'search_keywords'=>$data['search_keywords'],
                                        'contacts'=>$data['contacts'],
                                        'telephone'=>$data['telephone'],
                                        'wxnum'=>$data['wxnum'],
                                        'qqnum'=>$data['qqnum'],
                                        'logo'=>$data['logo'],
                                        'sertime'=>$data['sertime'],
                                        'pro_id'=>$data['pro_id'],
                                        'city_id'=>$data['city_id'],
                                        'area_id'=>$data['area_id'],
                                        'address'=>$data['address'],
                                        'lng'=>$latlon[0],
                                        'lat'=>$latlon[1],
                                        'fenxiao'=>$data['fenxiao'],
                                        'open_status'=>$data['open_status'],
                                        'id'=>$data[id]
                                    ));
                                    if($count !== false){
                                        $value = array('status'=>1,'mess'=>'保存信息成功');
                                    }else{
                                        $value = array('status'=>0,'mess'=>'保存信息失败');
                                    }
                                }else{
                                    $value = array('status'=>0,'mess'=>'地址坐标参数错误');
                                }
                            }else{
                                $value = array('status'=>0,'mess'=>'请选择区域，操作失败');
                            }
                        }else{
                            $value = array('status'=>0,'mess'=>'请选择区域，操作失败');
                        }
                    }else{
                        $value = array('status'=>0,'mess'=>'请选择区域，操作失败');
                    }
                }else{
                    $value = array('status'=>0,'mess'=>'找不到相关商店信息');
                }
            }
            return json($value);
        }else{
            if(input('shop_id')){
                $id = input('shop_id');

                if($id != 1){
                    $shops = Db::name('shops')->alias('a')->field('a.*,b.price,c.industry_name,c.remind,d.pro_name,f.city_name,u.area_name')->join('sp_shop_wallet b','a.id = b.shop_id','LEFT')->join('sp_industry c','a.indus_id = c.id','LEFT')->join('sp_province d','a.pro_id = d.id','LEFT')->join('sp_city f','a.city_id = f.id','LEFT')->join('sp_area u','a.area_id = u.id','LEFT')->where('a.id',$id)->find();
                    if($shops){
                        if(input('s')){
                            $this->assign('search',input('s'));
                        }
                        $industryres = Db::name('industry')->where('is_show',1)->field('id,industry_name')->order('sort asc')->select();
                        $prores = Db::name('province')->where('checked',1)->where('pro_zs',1)->field('id,pro_name,zm')->order('sort asc')->select();
                        $cityres = Db::name('city')->where('pro_id',$shops['pro_id'])->field('id,city_name,zm')->order('sort asc')->select();
                        $areares = Db::name('area')->where('city_id',$shops['city_id'])->field('id,area_name,zm')->select();
                        $this->assign('pnum', input('page'));
                        $this->assign('prores',$prores);
                        $this->assign('cityres',$cityres);
                        $this->assign('areares',$areares);
                        $this->assign('industryres',$industryres);
                        $this->assign('shops',$shops);
                        return $this->fetch();
                    }else{
                        $this->error('参数错误');
                    }
                }else{
                    $this->error('缺少参数');
                }
            }else{
                $this->error('缺少参数');
            }
        }

    }


    public function search(){
        $shop_id = input('shop_id');
        
        if(input('post.starttime') != ''){
            $shopsstarttime = strtotime(input('post.starttime'));
            cookie('shopsstarttime',$shopsstarttime,3600);
        }
    
        if(input('post.endtime') != ''){
            $shopsendtime = strtotime(input('post.endtime'));
            cookie('shopsendtime',$shopsendtime,3600);
        }

        $yearMeth = $this->getYearMeth($shopsstarttime,$shopsendtime);
        $newYerMeth = $this->getYearMeth($shopsendtime,time());
        $newYerMeth ='-'.$newYerMeth;
        $monthTotalTurnover = [];

        // 月销售量
        for ($i=0; $i <= $yearMeth; $i++) {
            $startTimeY = date('Y',strtotime(($newYerMeth-$i).' month'));
            $startTimeM = date('m',strtotime(($newYerMeth-$i).' month'));

            $startTime = $startTimeY.'-'.$startTimeM.'-1';
            $endTime = $startTimeY.'-'.$startTimeM.'-'.date('t',strtotime(($newYerMeth-$i).' month'));

            $totalTurnover = Db::name('order')->where('state','1')->where('shop_id',$shop_id)->whereTime('addtime', 'between', [$startTime, $endTime])->sum('goods_price');

            $monthTotalTurnover[date('y',strtotime(($newYerMeth-$i).' month')).'年'.date('m',strtotime(($newYerMeth-$i).' month')).'月']= $totalTurnover;
        }



        if(cookie('shopsstarttime') != ''){
            $this->assign('starttime',cookie('shopsstarttime'));
        }
    
        if(cookie('shopsendtime') != ''){
            $this->assign('endtime',cookie('shopsendtime'));
        }

        $this->assign('monthTotalTurnover', $monthTotalTurnover);// 赋值数据集
        $this->assign('shop_id', $shop_id);// 赋值数据集
        if(request()->isAjax()){
            return $this->fetch('info_ajaxpage');
        }else{
            return $this->fetch('info');
        }
    }

    public function getYearMeth($shopsstarttime,$shopsendtime){

        $y = date('y',$shopsstarttime);
        $ys = date('y',$shopsendtime);
        $m = date('m',$shopsstarttime);
        $ms = date('m',$shopsendtime);
        $chaY = $ys - $y;
        $chaM = 12-$m + $ms;
        $yearMeth = $chaM + (($chaY - 1) * 12);
        return $yearMeth;
    }

}
