<?php
namespace app\agent\controller;
use app\agent\controller\Common;
use think\Db;

class ShopTxmx extends Common{
    
    public function lst(){
        $user_id = session('agentuser_id');
        $list = Db::name('withdraw')->where('user_id',$user_id)->order('create_time desc')->paginate(25);
        $page = $list->render();
    
        if(input('page')){
            $pnum = input('page');
        }else{
            $pnum = 1;
        }
    
        $this->assign(array(
            'list'=>$list,
            'page'=>$page,
            'pnum'=>$pnum
        ));
        if(request()->isAjax()){
            return $this->fetch('ajaxpage');
        }else{
            return $this->fetch('lst');
        }
    }
    
    
    public function info(){
        if(input('tx_id')){
            $user_id = session('agentuser_id');
            $tx_id = input('tx_id');
            $txs = Db::name('withdraw')->where('id',$tx_id)->where('user_id',$user_id)->find();

            if($txs){
                $wallets = Db::name('wallet')->where('user_id',$user_id)->find();
                $txs['wallet_price'] = $wallets['price'];
                if(input('s')){
                    $this->assign('search',input('s'));
                }
                $this->assign('txs',$txs);
                return $this->fetch();
            }else{
                $this->error('参数错误');
            }
        }else{
            $this->error('缺少参数');
        }
    }
    
    public function search(){
        $shop_id = session('shopsh_id');
        
        if(input('post.keyword') != ''){
            cookie('shoptx_keyword',input('post.keyword'),7200);
        }else{
            cookie('shoptx_keyword',null);
        }
    
        if(input('post.tx_zt') != ''){
            cookie("shoptx_zt", input('post.tx_zt'), 7200);
        }
    
        if(input('post.starttime') != ''){
            $shoptxstarttime = strtotime(input('post.starttime'));
            cookie('shoptxstarttime',$shoptxstarttime,3600);
        }
    
        if(input('post.endtime') != ''){
            $shoptxendtime = strtotime(input('post.endtime'));
            cookie('shoptxendtime',$shoptxendtime,3600);
        }
    
        $where = array();
        
        $where['shop_id'] = $shop_id;
        
        if(cookie('shoptx_zt') != ''){
            $shoptx_zt = (int)cookie('shoptx_zt');
            if($shoptx_zt != 0){
                switch($shoptx_zt){
                    //待审核
                    case 1:
                        $where['checked'] = 0;
                        $where['complete'] = 0;
                        break;
                        //待打款
                    case 2:
                        $where['checked'] = 1;
                        $where['complete'] = 0;
                        break;
                        //已完成
                    case 3:
                        $where['checked'] = 1;
                        $where['complete'] = 1;
                        break;
                        //打款失败
                    case 4:
                        $where['checked'] = 1;
                        $where['complete'] = 2;
                        break;
                        //审核未通过
                    case 5:
                        $where['checked'] = 2;
                        $where['complete'] = 0;
                        break;
                }
            }
        }
    
        if(cookie('shoptx_keyword')){
            $where['tx_number'] = cookie('shoptx_keyword');
        }
    
        if(cookie('shoptxendtime') && cookie('shoptxstarttime')){
            $where['time'] = array(array('egt',cookie('shoptxstarttime')), array('lt',cookie('shoptxendtime')));
        }
    
        if(cookie('shoptxstarttime') && !cookie('shoptxendtime')){
            $where['time'] = array('egt',cookie('shoptxstarttime'));
        }
    
        if(cookie('shoptxendtime') && !cookie('shoptxstarttime')){
            $where['time'] = array('lt',cookie('shoptxendtime'));
        }
    
        $list =  Db::name('shop_txmx')->where($where)->order('time desc')->paginate(50);
        $page = $list->render();
    
        if(input('page')){
            $pnum = input('page');
        }else{
            $pnum = 1;
        }
        $search = 1;
    
        if(cookie('shoptxstarttime') != ''){
            $this->assign('starttime',cookie('shoptxstarttime'));
        }
    
        if(cookie('shoptxendtime') != ''){
            $this->assign('endtime',cookie('shoptxendtime'));
        }
    
        if(cookie('shoptx_keyword') != ''){
            $this->assign('keyword',cookie('shoptx_keyword'));
        }
    
        if(cookie('shoptx_zt') != ''){
            $this->assign('tx_zt',cookie('shoptx_zt'));
        }
    
        $this->assign('search',$search);
        $this->assign('pnum', $pnum);
        $this->assign('list', $list);// 赋值数据集
        $this->assign('page', $page);// 赋值分页输出
        if(request()->isAjax()){
            return $this->fetch('ajaxpage');
        }else{
            return $this->fetch('lst');
        }
    }
    
    
    public function index(){
        $user_id = session('agentuser_id');
        $wallets = Db::name('wallet')->where('user_id',$user_id)->field('price')->find();
        $cards = Db::name('bank_card')->where('user_id',$user_id)->field('*')->find();
        if($wallets){
            $this->assign('price',$wallets['price']);
            $this->assign('cards',$cards);
            $this->assign('txje',get_config_value('balance_withdraw_min_money'));
            return $this->fetch();
        }elseif(!$wallets){
            $this->error('参数错误');
        }
    }
    
    public function tixian(){
        if(request()->isPost()){

            $user_id = session('agentuser_id');
            $paypwd = Db::name('member')->where('id',$user_id)->value('paypwd');
            if(!$paypwd){
                datamsg(5,'请先设置支付密码');
            }

            if(!input('post.price')){
                datamsg(0,'请填写提现金额');
            }

            if(!input('post.pay_type')){
                datamsg(0,'请选择提现方式');
            }

            $payType = input('post.pay_type');
            $cards = Db::name('bank_card')->where('user_id',$user_id)->find();
            switch ($payType){
                case '1':
                    if(empty($cards['name']) || empty($cards['card_number']) || empty($cards['bank_name']) ){
                        datamsg(0,'请先完善银行卡信息');
                    }
                    break;
                case '2':
                    if(empty($cards['usdt_trc20'])){
                        datamsg(0,'请先完善USDT TRC20信息');
                    }
                    break;
                case '3':
                    if(empty($cards['usdt_erc20'])){
                        datamsg(0,'请先完善USDT ERC20信息');
                    }
                    break;
                case '4':
                    if(empty($cards['eth'])){
                        datamsg(0,'请先完善ETH信息');
                    }
                    break;
                default:
                    datamsg(0,'收款方式参数错误');
            }


            $pay_password = input('post.pay_password');
            if($paypwd != md5($pay_password)){
                datamsg(0,'支付密码错误');
            }


            if(!preg_match("/(^[1-9]([0-9]+)?(\\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\\.[0-9]([0-9])?$)/", input('post.price'))){
                datamsg(0,'提现金额格式错误');
            }

            $price = input('post.price');
            $webconfig = $this->webconfig;

            if($price < $webconfig['balance_withdraw_min_money']){
                datamsg(0,'每次最少提现'.$webconfig['balance_withdraw_min_money'].'元');
            }

            $wallets = Db::name('wallet')->where('user_id',$user_id)->find();
            if(!$wallets || $wallets['price'] < $price){
                datamsg(0,'您的钱包余额不足，提现失败');
            }


            $txmxnum = Db::name('withdraw')->where('user_id',$user_id)->where('checked','<>',2)->where('complete','<>',2)->whereTime('create_time', 'month')->count();
            if($txmxnum > $webconfig['balance_withdraw_max_number']){
                datamsg(0,'每月最多提现'.$webconfig['balance_withdraw_max_number'].'次');
            }



            $tx_number = 'TX'.$user_id.date('YmdHis').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);


            $txmxs = Db::name('withdraw')->where('tx_number',$tx_number)->find();
            if(!$txmxs){

                // 启动事务
                Db::startTrans();
                try{
                    $txId = Db::name('withdraw')->insertGetId(array(
                        'tx_number'=>$tx_number,
                        'price'=>$price,
                        'create_time'=>time(),
                        'user_id'=>$user_id,
                        'card_number'=>$cards['card_number'],
                        'zs_name'=>$cards['name'],
                        'bank_name'=>$cards['bank_name'],
                        'shengshiqu'=> '',
                        'branch_name'=>$cards['branch_name'],
                        'pay_type'=> $payType,
                        'usdt_trc20' =>$cards['usdt_trc20'],
                        'usdt_erc20' =>$cards['usdt_erc20'],
                        'eth' =>$cards['eth'],
                    ));
                    Db::name('wallet')->where('id',$wallets['id'])->setDec('price', $price);
                    Db::name('detail')->insert(array('de_type'=>2,'zc_type'=>1,'price'=>$price,'tx_id'=>$txId,'user_id'=>$user_id,'wat_id'=>$wallets['id'],'time'=>time()));
                    // 提交事务
                    Db::commit();
                    $value = array('status'=>1,'mess'=>'提现申请成功，平台将尽快处理');
                } catch (\Exception $e) {
                    // 回滚事务
                    Db::rollback();
                    $value = array('status'=>0,'mess'=>'申请提现失败');
                }
            }else{
                $value = array('status'=>0,'mess'=>'申请提现失败，请重试');
            }









            return json($value);
        }
    }
}
