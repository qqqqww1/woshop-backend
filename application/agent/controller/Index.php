<?php
/*
 * @Descripttion:
 * @Copyright: 武汉一一零七科技有限公司©版权所有
 * @Contact: QQ:2487937004
 * @Date: 2020-03-10 02:15:07
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2020-08-03 16:14:50
 */
namespace app\agent\controller;
use app\agent\controller\Common;
use think\Db;

class Index extends Common
{
    public function index(){
        $webconfig = $this->webconfig;
        $this->assign('webconfig',$webconfig);
        return $this->fetch();
    }

    public function index_v3(){
        $shop_id = session('shopsh_id');
        $agent_id = session('agent_id');

        $agent_user = Db::name('agent')->where('id',$agent_id)->find();
        $wallets = Db::name('shop_wallet')->where('shop_id',$shop_id)->find();
        $ordercount = Db::name('order')->where('shop_id',$shop_id)->count();

        $nowtime = time();
        $year = date('Y',time());
        $year2 = $year+1;
        $month = date('m',time());
        $month2 = $month+1;

        $day = date('d',time());
        $nowmonth = strtotime($year.'-'.$month.'-01 00:00:00');
        $lastmonth = strtotime($year.'-'.$month2.'-01 00:00:00');
        $nowyear = strtotime($year.'-01-01 00:00:00');
        $lastyear = strtotime($year2.'-01-01 00:00:00');

        // 全部已支付订单
        $order_num = Db::name('order')->where('shop_id',$shop_id)->where('state',1)->count();
        // 本月所有订单
        $month_order_num_all = Db::name('order')->where('shop_id',$shop_id)->whereTime('addtime','m')->count();
        // 本月已支付订单
        $month_order_num = Db::name('order')->where('shop_id',$shop_id)->where('state',1)->whereTime('addtime','m')->count();
        // 本月已成交订单
        $deal_num = Db::name('order')->where('shop_id',$shop_id)->whereTime('addtime','m')->where('order_status',1)->count();
        // 本月待支付订单
        $dai_num = Db::name('order')->where('shop_id',$shop_id)->whereTime('addtime','m')->where('state',0)->count();
        // 本月售后订单
        $shou_num = Db::name('order')->where('shop_id',$shop_id)->whereTime('addtime','m')->where('shouhou',1)->count();

        if($month_order_num_all > 0){
            $deal_lv = sprintf("%.2f",$deal_num/$month_order_num_all)*100;
            $dai_lv = sprintf("%.2f",$dai_num/$month_order_num_all)*100;
            $shou_lv = sprintf("%.2f",$shou_num/$month_order_num_all)*100;
        }else{
            $deal_lv = 0;
            $dai_lv = 0;
            $shou_lv = 0;
        }

        $month_salenum = Db::name('order_goods')->alias('a')->join('sp_order b','a.order_id=b.id','INNER')->where('b.shop_id',$shop_id)->where('b.state',1)->where('b.addtime','egt',$nowmonth)->where('b.addtime','lt',$lastyear)->count();
        $month_tuinum = Db::name('order_goods')->alias('a')->join('sp_order b','a.order_id = b.id','INNER')->where('b.shop_id',$shop_id)->where('a.th_status','in','1,2,3,4')->where('b.state',1)->where('b.addtime','egt',$nowmonth)->where('b.addtime','lt',$lastyear)->count();
        $month_huannum = Db::name('order_goods')->alias('a')->join('sp_order b','a.order_id = b.id','INNER')->where('b.shop_id',$shop_id)->where('a.th_status','in','5,6,7,8')->where('b.state',1)->where('b.addtime','egt',$nowmonth)->where('b.addtime','lt',$lastyear)->count();

        $monthSalenumStr = '';
        $monthTuinumStr = '';
        $monthHuannumStr = '';
        // 全年各月销售量、退款量、换货量
        for ($i=1; $i <= 12; $i++) {
            $monthSalenum = Db::name('order_goods')->alias('a')->join('sp_order b','a.order_id=b.id','INNER')->where('b.shop_id',$shop_id)->where('b.state',1)->where("FROM_UNIXTIME(b.addtime,'%m') = ".$i)->where('b.addtime','>',$nowyear)->count();
            $monthTuinum = Db::name('order_goods')->alias('a')->join('sp_order b','a.order_id = b.id','INNER')->where('b.shop_id',$shop_id)->where('a.th_status','in','1,2,3,4')->where('b.state',1)->where("FROM_UNIXTIME(b.addtime,'%m') = ".$i)->where('b.addtime','>',$nowyear)->count();
            $monthHuannum = Db::name('order_goods')->alias('a')->join('sp_order b','a.order_id = b.id','INNER')->where('b.shop_id',$shop_id)->where('a.th_status','in','5,6,7,8')->where('b.state',1)->where("FROM_UNIXTIME(b.addtime,'%m') = ".$i)->where('b.addtime','>',$nowyear)->count();
            $i < 12 ? $separator = ',' : $separator = '';
            $monthSalenumStr .= $monthSalenum.$separator;
            $monthTuinumStr .= $monthTuinum.$separator;
            $monthHuannumStr .= $monthHuannum.$separator;
        }

        // 总营业额
        $totalTurnover = Db::name('order')->where('state','1')->where('shop_id',$shop_id)->sum('goods_price');

        $this->assign('agent_user',$agent_user);
        $this->assign('agent_id',$agent_id);
        $this->assign('order_num',$order_num);
        $this->assign('month_order_num',$month_order_num);
        $this->assign('wallet_price',$wallets['price']);
        $this->assign('ordercount',$ordercount);
        $this->assign('deal_lv',$deal_lv);
        $this->assign('dai_lv',$dai_lv);
        $this->assign('shou_lv',$shou_lv);
        $this->assign('month_salenum',$month_salenum);
        $this->assign('month_tuinum',$month_tuinum);
        $this->assign('month_huannum',$month_huannum);
        $this->assign('totalTurnover',$totalTurnover);
        $this->assign('month',date('n',time()));
        $this->assign('year', $year);
        $this->assign('monthSalenumStr',$monthSalenumStr);
        $this->assign('monthTuinumStr',$monthTuinumStr);
        $this->assign('monthHuannumStr',$monthHuannumStr);
        return $this->fetch();
    }

}