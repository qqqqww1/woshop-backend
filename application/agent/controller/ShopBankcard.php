<?php
namespace app\agent\controller;
use app\agent\controller\Common;
use think\Db;

class ShopBankcard extends Common{
    
    public function info(){
        if(request()->isPost()){
            $user_id = session('agentuser_id');
            $data = input('post.');
            $data['user_id'] = $user_id;
            $result = $this->validate($data,'ShopBankcard');
            if(true !== $result){
                $value = array('status'=>0,'mess'=>$result);
            }else{
                $cards = Db::name('bank_card')->where('user_id',$user_id)->find();
                if($cards){
                    $count = Db::name('bank_card')->update(array(
                        'name'=>$data['name'],
                        'telephone'=>$data['telephone'],
                        'card_number'=>$data['card_number'],
                        'bank_name'=>$data['bank_name'],
                        'usdt_trc20'=>$data['usdt_trc20'],
                        'usdt_erc20'=>$data['usdt_erc20'],
                        'eth'=>$data['eth'],
                        'user_id'=>$data['user_id'],
                        'id'=>$cards['id']
                    ));
                    if($count !== false){
                        $value = array('status'=>1,'mess'=>'保存成功');
                    }else{
                        $value = array('status'=>0,'mess'=>'保存失败');
                    }
                }else{
                    $lastId = Db::name('bank_card')->insert(array(
                        'name'=>$data['name'],
                        'telephone'=>$data['telephone'],
                        'card_number'=>$data['card_number'],
                        'bank_name'=>$data['bank_name'],
                        'usdt_trc20'=>$data['usdt_trc20'],
                        'usdt_erc20'=>$data['usdt_erc20'],
                        'eth'=>$data['eth'],
                        'user_id'=>$data['user_id']
                    ));
                    if($lastId){
                        $value = array('status'=>1,'mess'=>'保存成功');
                    }else{
                        $value = array('status'=>0,'mess'=>'保存失败');
                    }
                }
            }
            return json($value);
        }else{
            $user_id = session('agentuser_id');
            $cards = Db::name('bank_card')->where('user_id',$user_id)->find();
            $this->assign('cards',$cards);
            return $this->fetch();
        }
    }
    
}