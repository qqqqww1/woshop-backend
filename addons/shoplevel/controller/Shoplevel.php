<?php

/**
 * Created by PhpStorm.
 * User: canme
 * Date: 2022-12-29
 * Time: 16:50
 */

namespace addons\shoplevel\controller;

use think\addons\Controller;
use think\Db;

class Shoplevel extends Controller
{
    /**
     * 商家配置信息
     */
    public function indexconfig()
    {
        if (request()->isPost()) {
            $param = input("post.");
            foreach (array_keys($param) as $key => $value) {
                Db::name("plugin_shoplevel_config")->where("enname", $value)->update(['value' => $param[$value]]);
            }
            return json(['status' => true, "msg" => '更改成功']);
        } else {
            $list = Db::name("plugin_shoplevel_config")->select();
            $this->assign("list", $list);
            return $this->fetch();
        }
    }


    /**
     * 商户配置信息
     */
    public function index()
    {
        if (request()->isPost()) {
            $param = request()->param();
            if (empty($param['level_name'])) {
                return json(['status' => false, 'msg' => '等级名称不能为空']);
            }
            if (empty($param['bond'])) {
                return json(['status' => false, 'msg' => '佣金金额不能为空']);
            }
            if (empty($param['rate'])) {
                return json(['status' => false, 'msg' => '佣金费率不能为空']);
            } else {
                if ($param['rate'] < 0 || $param['rate'] > 100) {
                    return json(['status' => false, 'msg' => "佣金金额必须大于0小于100"]);
                }
            }
            if (empty($param['level_desc'])) {
                return json(['status' => false, 'msg' => '说明不能为空']);
            }
            $plugin_shoplevel = Db::name("plugin_shoplevel")->where("id", $param['id'])->find();
            if (empty($plugin_shoplevel)) {
                return json(['status' => false, 'msg' => '没有找到佣金配置']);
            }
            $plugin_shoplevel['level_name'] = $param['level_name'];
            $plugin_shoplevel['bond'] = $param['bond'];
            $plugin_shoplevel['rate'] = $param['rate'];
            $plugin_shoplevel['updated_at'] = time();
            $plugin_shoplevel['level_desc'] = $param['level_desc'];
            $result = Db::name("plugin_shoplevel")->update($plugin_shoplevel);
            if ($result) {
                return json(['status' => true, 'msg' => '更新成功']);
            } else {
                return json(['status' => false, 'msg' => '更新失败']);
            }
        } else {
            $list = Db::name("plugin_shoplevel")->select();
            $this->assign("list", $list);
            return $this->fetch();
        }
    }


    /**
     * 用户提交记录信息
     */
    public function shopindex()
    {
        if (empty($filter)) {
            $filter = 0;
        }
        $params = input();
        if (isset($params['keyword'])) {
            if ($params['keyword'] != '') {
                cookie('keyword', $params['keyword'], 7200);
            } else {
                cookie('keyword', null);
            }
        }

        if (isset($params['admin_check'])) {
            if ($params['admin_check'] != 0) {
                cookie('admin_check', $params['admin_check'], 7200);
            } else {
                cookie('admin_check', 0);
            }
        }

        $keyword = cookie("keyword");
        if ($keyword) {
            $where['b.telephone'] = $keyword;
        }
        $admin_check = cookie("admin_check");
        if ($admin_check != 0) {
            $where['a.admin_check'] = $admin_check;
        }


        $list = Db::name('plugin_shoplevel_user')
            ->alias('a')
            ->field('a.*,c.phone user_phone,c.email,b.telephone shop_phone')
            ->join('shops b', 'a.shop_id = b.id', 'LEFT')
            ->join('member c', 'c.id = a.user_id', 'LEFT')
            ->where($where)
            ->order('a.id desc')
            ->paginate(10)
            ->each(function ($item, $k) {
                $item['user_phone'] = !empty($item['user_phone']) ? $item['user_phone'] : $item['email'];
                return $item;
            });
        $page = $list->render();
        if (input('page')) {
            $pnum = input('page');
        } else {
            $pnum = 1;
        }
        $this->assign(array(
            'filter' => $filter,
            'list' => $list,
            'page' => $page,
            'pnum' => $pnum,
            'keyword' => $keyword,
            'admin_check' => $admin_check
        ));
        return $this->fetch();
    }


    /**
     * 审核用户提交过来的充值记录
     */
    public function checked()
    {
        $params = input();
        if (request()->isAjax()) {

            if ($params['admin_check'] == 4) {
                if (empty($params['check_tips'])) {
                    return json(['status' => false, 'msg' => "拒绝理由必填"]);
                }
            }

            $shoplevel_user = Db::name("plugin_shoplevel_user")->where("id", $params['id'])->find();
            if ($shoplevel_user['admin_check'] != 1) {
                return json(['status' => false, 'msg' => "该状态不能审核"]);
            }

            if (empty($shoplevel_user)) {
                return json(['status' => false, 'msg' => "没有找到审核数据"]);
            }
            $shoplevel_user['check_tips'] = $params['check_tips'];
            $shoplevel_user['admin_check'] = $params['admin_check'];
            $shoplevel_user['status'] = $params['admin_check'] == 3 ? 1 : 0;
            $shoplevel_user['check_time'] = time();
            if (Db::name("plugin_shoplevel_user")->where("id", $params['id'])->update($shoplevel_user)) {
                return json(['status' => 1, 'msg' => "审核成功"]);
            } else {
                return json(['status' => 0, 'msg' => "审核失败"]);
            }
        } else {
            if (!isset($params['id']) || empty($params['id'])) {
                return "<h3 style='text-align: center'>系统出错，id不为空</h3>";
            }
            $list = Db::name('plugin_shoplevel_user')
                ->alias('a')
                ->field('a.*,c.phone user_phone,b.phone shop_phone')
                ->join('shop_admin b', 'a.shop_id = b.shop_id', 'LEFT')
                ->join('member c', 'c.id = a.user_id', 'LEFT')
                ->where("a.id", $params['id'])
                ->limit(1)
                ->find();
            if (empty($list)) {
                return "<h3 style='text-align: center'>没有找到用户记录</h3>";
            }
            $this->assign("list", $list);
            return $this->fetch();
        }
    }



    /**
     * 详情
     */
    public function info()
    {
        $params = input();
        if (!isset($params['id']) || empty($params['id'])) {
            return "<h3 style='text-align: center'>系统出错，id不为空</h3>";
        }
        $list = Db::name('plugin_shoplevel_user')
            ->alias('a')
            ->field('a.*,c.phone user_phone,b.phone shop_phone')
            ->join('shop_admin b', 'a.shop_id = b.shop_id', 'LEFT')
            ->join('member c', 'c.id = a.user_id', 'LEFT')
            ->where("a.id", $params['id'])
            ->limit(1)
            ->find();
        if (empty($list)) {
            return "<h3 style='text-align: center'>没有找到用户记录</h3>";
        }
        $this->assign("list", $list);
        return $this->fetch();
    }


    /**
     * 添加用户
     */
    public function add()
    {
        if (request()->post()) {
            $data = input();
            if (!isset($data['shoplevel_id']) || empty($data['shoplevel_id'])) {
                return ['status' => 0, 'mess' => '请选择等级'];
            }
            if (!isset($data['shop_id']) || empty($data['shop_id'])) {
                return ['status' => 0, 'mess' => '请选择店铺'];
            }
            $shoplevel = Db::name("plugin_shoplevel")->where("id", $data['shoplevel_id'])->find();
            if (!$shoplevel) {
                return ['status' => 0, 'mess' => '没有找到等级信息'];
            }
            $shops = Db::name("shops")->where("id", $data['shop_id'])->find();
            if (!$shops) {
                return ['status' => 0, 'mess' => '没有找到店铺信息'];
            }
            $member = Db::name("member")->where("shop_id", $shops['id'])->find();
            $isExitShopLevel = Db::name("plugin_shoplevel_user")->where(['status' => 1, 'admin_check' => 3, 'shop_id' => $shops['id']])->count();
            if ($isExitShopLevel) {
                return ['status' => 0, 'mess' => "当前店铺:{$shops['shop_name']}已经添加"];
            }

            $insertData['shoplevel_id'] = $shoplevel['id'];
            $insertData['user_id'] = $member ? $member['id'] : 0;
            $insertData['shop_id'] = $shops['id'];
            $insertData['level_name'] = $shoplevel['level_name'];
            $insertData['status'] = 1;
            $insertData['created_at'] = time();
            $insertData['updated_at'] = time();
            $insertData['admin_check'] = 3;
            $insertData['bond'] = $shoplevel['bond'];
            $insertData['rate'] = $shoplevel['rate'];
            $insertData['check_tips'] = "管理员后台添加";
            $insertData['check_time'] = time();
            $plugin_shoplevel_user = Db::name("plugin_shoplevel_user")->insert($insertData);
            if ($plugin_shoplevel_user) {
                return ['status' => 1, 'mess' => '添加成功'];
            } else {
                return ['status' => 0, 'mess' => '添加失败'];
            }
        } else {
            $plugin_shoplevel = Db::name("plugin_shoplevel")->select();
            $this->assign("shoplevel", $plugin_shoplevel);
            return $this->fetch();
        }
    }

    // 设置等级
    public function set_level()
    {
        $params = input();
        if (!isset($params['id']) || empty($params['id'])) {
            return "<h3 style='text-align: center'>系统出错，id不为空</h3>";
        }
        $list = Db::name('plugin_shoplevel_user')
            ->alias('a')
            ->field('a.*,c.phone user_phone,b.phone shop_phone')
            ->join('shop_admin b', 'a.shop_id = b.shop_id', 'LEFT')
            ->join('member c', 'c.id = a.user_id', 'LEFT')
            ->where("a.id", $params['id'])
            ->limit(1)
            ->find();
        if (empty($list)) {
            return "<h3 style='text-align: center'>没有找到用户记录</h3>";
        }
        $this->assign("list", $list);

        $plugin_shoplevel = Db::name("plugin_shoplevel")->select();
        $this->assign("levels", $plugin_shoplevel);
        $this->assign("id", $params['id']);
        return $this->fetch();
    }

    // 设置等级 
    public function set_level_post()
    {
        $params = input();
        if (request()->isAjax()) {
            if (!isset($params['id']) || empty($params['id'])) {
                return json(['status' => false, 'msg' => '系统出错，id不为空']);
            }
            if (!isset($params['shoplevel_id']) || empty($params['shoplevel_id'])) {
                return json(['status' => false, 'msg' => '请选择等级']);
            }
            $originShopLevel = Db::name("plugin_shoplevel_user")->where(['id' => $params['id']])->find();
            $shoplevel = Db::name("plugin_shoplevel")->where("id", $params['shoplevel_id'])->find();
            if (!$shoplevel) {
                return json(['status' => false, 'msg' => '没有找到等级信息']);
            }
            // 如果存在 则修改 status 为 1 ，修改等级id为对应 id
            if ($originShopLevel) {
                $originShopLevel['shoplevel_id'] = $shoplevel['id'];
                $originShopLevel['level_name'] = $shoplevel['level_name'];
                $originShopLevel['bond'] = $shoplevel['bond'];
                $originShopLevel['rate'] = $shoplevel['rate'];
                $originShopLevel['status'] = 1;
                $originShopLevel['updated_at'] = time();
                $result = Db::name("plugin_shoplevel_user")->update($originShopLevel);
                if ($result) {
                    return json(['status' => true, 'msg' => '设置成功']);
                } else {
                    return json(['status' => false, 'msg' => '设置失败']);
                }
            } else {
                // 如果不存在 报错
                return json(['status' => false, 'msg' => '没有找到商家等级信息']);
            }
        }
    }
}
