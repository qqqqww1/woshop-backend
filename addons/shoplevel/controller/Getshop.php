<?php
/**
 * Created by PhpStorm.
 * User: canme
 * Date: 2022-12-29
 * Time: 16:50
 */
namespace addons\shoplevel\controller;
use think\addons\Controller;
use think\Db;

class Getshop extends Controller
{
    public function lst(){
        $where['a.open_status'] = 1;
        $list = Db::name('shops')
            ->alias('a')
            ->field('a.id,a.shop_name,a.contacts,a.telephone,a.logo,b.industry_name')
            ->join('sp_industry b','a.indus_id = b.id','LEFT')
            ->where($where)->order('a.addtime desc')
            ->paginate(25);
        $page = $list->render();
        if(input('page')){
            $pnum = input('page');
        }else{
            $pnum = 1;
        }
        $this->assign(array(
            'pnum'=>$pnum,
            'list'=>$list,
            'page'=>$page,
        ));
        if(request()->isAjax()){
            return $this->fetch('ajaxpage');
        }else{
            return $this->fetch();
        }
    }


    //搜索
    public function search(){
        if(input('keyword') != ''){
            cookie('shop_keyword',input('keyword'),3600);
        }else{
            cookie('shop_keyword',null);
        }
        $where = array();

        $where['a.open_status'] = 1;
        if(cookie('shop_keyword')){
            $where['a.shop_name|a.telephone'] = array('like', '%' . cookie('shop_keyword') . '%');
        }
        $list = Db::name('shops')
            ->alias('a')
            ->field('a.id,a.shop_name,a.contacts,a.telephone,a.logo,b.industry_name')
            ->join('sp_industry b','a.indus_id = b.id','LEFT')
            ->where($where)
            ->order('a.addtime desc')
            ->paginate(25);
        $page = $list->render();

        if(input('page')){
            $pnum = input('page');
        }else{
            $pnum = 1;
        }
        $search = 1;
        $this->assign('keyword',cookie('shop_keyword'));
        $this->assign('list', $list);
        $this->assign('page', $page);
        $this->assign('pnum', $pnum);
        $this->assign('search',$search);
        if(request()->isAjax()){
            return $this->fetch('ajaxpage');
        }else{
            return $this->fetch('lst');
        }
    }

}