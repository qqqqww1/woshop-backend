<?php

return [
    'display' => [
        'title' => '是否显示:',
        'type' => 'radio',
        'options' => [
            '1' => '显示',
            '0' => '不显示'
        ],
        'value' => [
            "name"=>"shoplevel",
            "title"=> "订单返现 商户升级(付费)",
            "intro"=>"2000元",
            "author"=> "官方",
            "version"=> "1.0.0",
            "category_id"=>"1",
            "status"=>"0",
            "shoplevel" => [
                "pri_name"=>"订单返现 商户升级(付费)",          //权限名称
                "mname"=> "shoplevel",              //模块名称
                "cname"=> "Shoplevel",              //控制器名称
                "aname"=> "shoplevel",              //操作方法名称
                "fwname"=> "shoplevel",             //控制器访问别名
                "icon"=> "fa fa-globe",
                "pid"=>"153",
                "status"=> "1",             //状态
                "sort"=> "40",            //排序
                "type"=>"1"               //代表插件
            ],
            "one" => [
                "pri_name"=>"订单返现",
                "mname"=> "shoplevel",
                "cname"=> "Shoplevel",
                "aname"=> "indexconfig",
                "fwname"=> "shoplevel",
                "status"=> "1",
                "sort"=>"1",
                "type"=>"1"
            ],
            "two" => [
                "pri_name"=>"商家等级说明",
                "mname"=> "shoplevel",
                "cname"=> "Shoplevel",
                "aname"=> "index",
                "fwname"=> "shoplevel",
                "status"=> "1",
                "sort"=>"2",
                "type"=>"1"
            ],
            "three" => [
                "pri_name"=>"商家等级审核",
                "mname"=> "shoplevel",
                "cname"=> "Shoplevel",
                "aname"=> "shopindex",
                "fwname"=> "shoplevel",
                "status"=> "1",
                "sort"=>"3",
                "type"=>"1"
            ],

        ]
    ]
];

