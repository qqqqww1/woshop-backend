<?php
namespace addons\shoplevel;	// 注意命名空间规范

use think\Db;
use think\Addons;
use think\Exception;

/**
 * 插件测试
 * @author byron sampson
 */
class Shoplevel extends Addons	// 需继承think\addons\Addons类
{
	// 该插件的基础信息
    public $info = [

    ];

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
	    $id=input('id');
	    $name=db('plugin')->where('id',$id)->value('name');
	    $configRes=$this->getConfig();
        // 启动事务
        Db::startTrans();
	    try{
            if(!isset($configRes['display'][$name]) || empty($configRes['display'][$name])) {
                throw new Exception("没有找到配置信息");
            }


            //生成商家等级信息表
            $db_prefix = config("database.prefix");
            $db_name = config("database.database");


            $db_table_plugin_shoplevel_config = "{$db_prefix}plugin_shoplevel_config";
            $is_exit_shoplevel_config = Db::query("select * from information_schema.TABLES where TABLE_SCHEMA='{$db_name}' AND TABLE_NAME='{$db_table_plugin_shoplevel_config}'");//plugin_shoplevel
            //判断数据表是否存在
            if(empty($is_exit_shoplevel_config)) {
                $sql_config = "CREATE TABLE IF NOT EXISTS `{$db_table_plugin_shoplevel_config}` (
                    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                    `cname` VARCHAR (100) DEFAULT NULL COMMENT '配置名称',
                    `enname` VARCHAR (100) DEFAULT '' COMMENT '配置值',
                    `type` tinyint (10) DEFAULT '0' COMMENT '类型 0文本框 1文本域 2单选框 3复选框 4下拉框 5图片' ,
                    `values` VARCHAR (150) DEFAULT '' COMMENT '返佣比例 单位百分比 例如填入2即2%',
                    `value` VARCHAR (150) NOT NULL COMMENT '默认值',
                    `sort` int (10) NOT NULL COMMENT '排序',
                    `remark` VARCHAR (255) NOT NULL COMMENT '备注',
                      PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商家等级信息表';";
                $plugin_shoplevelResult = Db::execute($sql_config);
                if ($plugin_shoplevelResult) {
                    throw new Exception("生成商家等级数据表失败");
                }
                $shop_level_config = [
                    ['id'=>NULL,'cname' => '付款码', 'enname' => 'pay_code_img', 'type' => '5', 'values' => '', 'value' => '', 'sort' => 1,'remark'=>'商户充值付款'],
                    ['id'=>NULL,'cname' => '付款说明', 'enname' => 'pay_code_desc', 'type' => '1', 'values' => '', 'value' => '请和管理员联系好在进行汇款，汇款完成请通知管理员进行审核', 'sort' => 2,'remark'=>'商户付款说明']
                ];
                $insertLevelConfigAll = Db::name("plugin_shoplevel_config")->insertAll($shop_level_config);
                if (!$insertLevelConfigAll) {
                    throw new Exception("生成商家配置数据表失败");
                }
            }


            $db_table_plugin_shoplevel = "{$db_prefix}plugin_shoplevel";
            $is_exit_shoplevel = Db::query("select * from information_schema.TABLES where TABLE_SCHEMA='{$db_name}' AND TABLE_NAME='{$db_table_plugin_shoplevel}'");//plugin_shoplevel
            //判断数据表是否存在
            if(empty($is_exit_shoplevel)) {
                $sql_shoplevel = "CREATE TABLE IF NOT EXISTS `{$db_table_plugin_shoplevel}` (
                    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                    `level_id` int (11) DEFAULT NULL COMMENT '等级',
                    `level_name` VARCHAR (100) DEFAULT '' COMMENT '等级名称',
                    `bond` VARCHAR (10) DEFAULT '' COMMENT '保证金金额' ,
                    `rate` VARCHAR (10) DEFAULT '' COMMENT '返佣比例 单位百分比 例如填入2即2%',
                    `created_at` int (11) NOT NULL COMMENT '创建时间',
                    `updated_at` int (11) NOT NULL COMMENT '修改时间',
                    `level_desc` VARCHAR (255) DEFAULT '' COMMENT '等级说明',
                      PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商家等级信息表';";
                $plugin_shoplevelResult = Db::execute($sql_shoplevel);
                if ($plugin_shoplevelResult) {
                    throw new Exception("生成商家等级数据表失败");
                }
                $shop_level_list = [
                    ['level_id' => 1, 'level_name' => 'LV1', 'bond' => '1000', 'rate' => '2', 'created_at' => time(), 'updated_at' => time(),'level_desc'=>'缴纳保证金1000可以获得订单佣金的2%'],
                    ['level_id' => 2, 'level_name' => 'LV2', 'bond' => '2000', 'rate' => '4', 'created_at' => time(), 'updated_at' => time(),'level_desc'=>'缴纳保证金2000可以获得订单佣金的4%'],
                    ['level_id' => 3, 'level_name' => 'LV3', 'bond' => '3000', 'rate' => '5', 'created_at' => time(), 'updated_at' => time(),'level_desc'=>'缴纳保证金3000可以获得订单佣金的5%'],
                    ['level_id' => 4, 'level_name' => 'LV4', 'bond' => '4000', 'rate' => '8', 'created_at' => time(), 'updated_at' => time(),'level_desc'=>'缴纳保证金4000可以获得订单佣金的8%'],
                    ['level_id' => 5, 'level_name' => 'LV5', 'bond' => '5000', 'rate' => '10', 'created_at' => time(), 'updated_at' => time(),'level_desc'=>'缴纳保证金5000可以获得订单佣金的10%']
                ];
                $insertAll = Db::name("plugin_shoplevel")->insertAll($shop_level_list);
                if (!$insertAll) {
                    throw new Exception("生成商户数据表失败");
                }
            }

            //生成商家信息充值记录表
            $db_table_plugin_shoplevel_user = "{$db_prefix}plugin_shoplevel_user";
            $is_exit_shoplevel_user = Db::query("select * from information_schema.TABLES where TABLE_SCHEMA='{$db_name}' AND TABLE_NAME='{$db_table_plugin_shoplevel_user}'");//plugin_shoplevel
            //判断数据表是否存在
            if(empty($is_exit_shoplevel_user)) {
                $sql_user = " CREATE TABLE IF NOT EXISTS `{$db_table_plugin_shoplevel_user}` (
                    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                    `shoplevel_id` int (11) DEFAULT NULL COMMENT '等级id',
                    `user_id` int (11) DEFAULT NULL COMMENT '用户id',
                    `shop_id` int (11) DEFAULT NULL COMMENT '对应的用户店铺id',
                    `level_name` VARCHAR (100) DEFAULT '' COMMENT '等级名称',
                    `status` tinyint (1) DEFAULT '1' COMMENT '当前等级是否可用0不可用 1可用',
                    `is_deleted` tinyint (1) DEFAULT '0' COMMENT '是否删除0正常 1删除',
                    `created_at` int (11) DEFAULT NULL COMMENT '创建时间',
                    `updated_at` int (11) DEFAULT NULL COMMENT '修改时间',
                    `voucher_img` VARCHAR (255) DEFAULT '' COMMENT '凭证图片',
                    `admin_check` tinyint (1) DEFAULT '0' COMMENT '0待提交凭证 1已提交 3通过 4拒绝',
                    `bond` VARCHAR (10) DEFAULT '' COMMENT '保证金金额',
                    `rate` VARCHAR (10) DEFAULT '' COMMENT '返佣比例 单位百分比 例如填入2即2%',
                    `check_tips` VARCHAR (255) DEFAULT '' COMMENT '审核备注',
                    `check_time` int (11) DEFAULT NULL COMMENT '后台审核时间',
                      PRIMARY KEY (`id`),
                      UNIQUE INDEX `shoplevel_id`(`shoplevel_id`, `shop_id`) USING BTREE COMMENT '店铺等级唯一索引'
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商户等级信息表记录表';";
                $plugin_shoplevel_userResult = Db::execute($sql_user);
                if ($plugin_shoplevel_userResult) {
                    throw new Exception("生成数据表失败");
                }
            }




            $pri_name=($configRes['display'][$name]['pri_name']);
            $find=db('privilege')->where('pri_name',$pri_name)->find();
            if(empty($find)) {
                $privilege = db('privilege')->insertGetId($configRes['display'][$name]);
                if (!$privilege) {
                    throw new Exception("插件权限已经存在");
                }

                $one = $configRes['display']['one'];
                if ($one) {
                    $one['pid'] = $privilege;
                    $oneResult = db('privilege')->insert($one);
                    if (!$oneResult) {
                        throw new Exception("生成商家等级权限失败");
                    }
                }

                $two = $configRes['display']['two'];
                if ($two) {
                    $two['pid'] = $privilege;
                    $twoResult = db('privilege')->insert($two);
                    if (!$twoResult) {
                        throw new Exception("生成商家等级权限节点失败");
                    }
                }

                $three = $configRes['display']['three'];
                if ($three) {
                    $three['pid'] = $privilege;
                    $threeResult = db('privilege')->insert($three);
                    if (!$threeResult) {
                        throw new Exception("生成商家等级权限节点失败");
                    }
                }
            }


            Db::commit();
            return ['status'=>1,'mess'=>"插件已存在"];
	    }catch (\Exception $e) {
            Db::rollback();
            return ['status'=>0,'mess'=>$e->getMessage()];
	    }
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
	    $id=input('id');
	    $name=db('plugin')->where('id',$id)->value('name');

	    try {
			db('privilege')->where(['mname'=>$name,'type'=>1])->delete();
		    return ['status'=>1];
	    }catch (\Exception $e) {
            return ['status'=>0,'mess'=>$e->getMessage()];
	    }

    }

    /**
     * 实现的langhook钩子方法
     * @return mixed
     */
    public function langhook($param)
    {
//        dump(1);die;
		// 调用钩子时候的参数信息
        print_r($param);
		// 当前插件的配置信息，配置信息存在当前目录的config.php文件中，见下方
        print_r($this->getConfig());
		// 可以返回模板，模板文件默认读取的为插件目录中的文件。模板名不能为空！
        return $this->fetch('info');
    }


	//获取插件的配置文件
	public function getPlugInfo($plug,$type="/info.json"){

		$plugPath=ROOT_PATH."addons/".$plug.$type;
		// dump($plugPath);die;
		if(is_file($plugPath)){
			$plugInfo=file_get_contents($plugPath);
			return $plugInfo;
		}
	}
}